"""
University of Copenhagen
Master Thesis

Deep Learning Pipeline For Iceberg Detection

Darrick Joo Jian Wen (NGC655)
"""

import cv2
import numpy as np
from keras.utils import Sequence
from extras.nn_util_v3 import BoundBox, bbox_iou
from extras.image_util_v3 import apply_random_scale_and_crop, random_distort_image, random_flip, correct_bounding_boxes

class BatchGenerator(Sequence):
    def __init__(self,
                 images,
                 anchors,
                 labels,  # Labels, just class names
                 downsample=32,  # Ratio between network input's size and network output's size, 32 for YOLOv3
                 max_box_per_image=30,  # Max number of boxes in the image
                 batch_size=1,  # Training batch size
                 min_net_size=480,  # The smallest image size to train on (Taken from config file usually)
                 max_net_size=608,  # The largest image size to train on (Taken from config file usually)
                 shuffle=True,  # Shuffle the images
                 jitter=True,  # Apply preprocessing to images (Flipping, scaling etc) 0.0-1.0, True = 1.0
                 norm=None  # Function to normalize image info. Usually just f(x): x / 255.
                 ):
        self.images = images
        self.batch_size = batch_size
        self.labels = labels
        self.labels_length = len(self.labels)
        self.downsample = downsample
        self.max_box_per_image = max_box_per_image
        self.min_net_size = (min_net_size // self.downsample) * self.downsample
        self.max_net_size = (max_net_size // self.downsample) * self.downsample
        self.shuffle = shuffle
        self.jitter = jitter
        self.norm = norm
        self.anchors = [BoundBox(0, 0, anchors[2 * i], anchors[2 * i + 1]) for i in range(len(anchors) // 2)]
        self.anchors_length = len(self.anchors)
        self.net_h = 416
        self.net_w = 416

        if shuffle:
            np.random.shuffle(self.images)

    def __len__(self):
        """
        Get number of times per epoch
        :return:
        """
        return int(np.ceil(float(len(self.images)) / self.batch_size))

    def get_length(self):
        """
        Get number of images
        :return: Number of images
        """
        return len(self.images)

    def __getitem__(self, idx):
        """
        Get item at idx
        :param idx: Index of image
        :return:
        """
        # Get image input size, change every 10 batches
        net_h, net_w = self._get_net_size(idx)

        # Grid size after downsampling
        base_grid_h, base_grid_w = net_h // self.downsample, net_w // self.downsample

        # Determine the first and the last indices of the batch
        l_bound = idx * self.batch_size
        r_bound = (idx + 1) * self.batch_size

        if r_bound > len(self.images):
            r_bound = len(self.images)
            l_bound = r_bound - self.batch_size

        # Input images
        x_batch = np.zeros((self.batch_size, net_h, net_w, 3))

        # List of ground truth boxes
        truth_batch = np.zeros((self.batch_size, 1, 1, 1, self.max_box_per_image, 4))

        # Initialize the inputs and the outputs
        # Desired network output 1
        yolo_1 = np.zeros((self.batch_size, 1 * base_grid_h, 1 * base_grid_w, self.anchors_length // 3,
                           4 + 1 + self.labels_length))
        # Desired network output 2
        yolo_2 = np.zeros((self.batch_size, 2 * base_grid_h, 2 * base_grid_w, self.anchors_length // 3,
                           4 + 1 + self.labels_length))
        # Desired network output 3
        yolo_3 = np.zeros((self.batch_size, 4 * base_grid_h, 4 * base_grid_w, self.anchors_length // 3,
                           4 + 1 + self.labels_length))
        # Desired network output 4
        # yolo_4 = np.zeros((self.batch_size, 8 * base_grid_h, 8 * base_grid_w, self.anchors_length // 4,
        #                    4 + 1 + self.labels_length))
        # yolos = [yolo_4, yolo_3, yolo_2, yolo_1]
        yolos = [yolo_3, yolo_2, yolo_1]

        dummy_yolo_1 = np.zeros((self.batch_size, 1))
        dummy_yolo_2 = np.zeros((self.batch_size, 1))
        dummy_yolo_3 = np.zeros((self.batch_size, 1))
        # dummy_yolo_4 = np.zeros((self.batch_size, 1))

        instance_count = 0
        true_box_index = 0

        # Do the logic to fill in the inputs and the output
        for training_image in self.images[l_bound:r_bound]:
            # Augment input image and fix object's position and size
            image, all_objects = self._aug_image(training_image, net_h, net_w)

            for object in all_objects:
                max_anchor = None
                max_index = -1
                max_iou = -1

                shifted_box = BoundBox(0, 0,
                                       object['xmax'] - object['xmin'],
                                       object['ymax'] - object['ymin'])

                # Find best anchor box for this object
                for anchor_index, anchor in enumerate(self.anchors):
                    # Get intersection over union
                    iou = bbox_iou(shifted_box, anchor)

                    if max_iou < iou:
                        max_anchor = anchor
                        max_index = anchor_index
                        max_iou = iou

                # Determine which yolo to be responsible for this bounding box
                # yolo = yolos[max_index // 4]
                yolo = yolos[max_index // 3]
                grid_h, grid_w = yolo.shape[1:3]

                # Determine the position of the bounding box on the grid
                center_x = .5 * (object['xmin'] + object['xmax'])
                center_x = center_x / float(net_w) * grid_w  # sigma(t_x) + c_x
                center_y = .5 * (object['ymin'] + object['ymax'])
                center_y = center_y / float(net_h) * grid_h  # sigma(t_y) + c_y

                # Determine the sizes of the bounding box
                w = np.log((object['xmax'] - object['xmin']) / float(max_anchor.xmax))  # t_w
                h = np.log((object['ymax'] - object['ymin']) / float(max_anchor.ymax))  # t_h

                box = [center_x, center_y, w, h]

                # Determine the index of the label
                object_index = self.labels.index(object['name'])

                # Determine the location of the cell responsible for this object
                grid_x = int(np.floor(center_x))
                grid_y = int(np.floor(center_y))

                # Assign ground truth x, y, w, h, confidence and class probs to y_batch (y_true)
                # yolo[instance_count, grid_y, grid_x, max_index % 4] = 0
                # yolo[instance_count, grid_y, grid_x, max_index % 4, 0:4] = box
                # yolo[instance_count, grid_y, grid_x, max_index % 4, 4] = 1.
                # yolo[instance_count, grid_y, grid_x, max_index % 4, 5 + object_index] = 1
                yolo[instance_count, grid_y, grid_x, max_index % 3] = 0
                yolo[instance_count, grid_y, grid_x, max_index % 3, 0:4] = box
                yolo[instance_count, grid_y, grid_x, max_index % 3, 4] = 1.
                yolo[instance_count, grid_y, grid_x, max_index % 3, 5 + object_index] = 1

                # Assign the true box to truth_batch
                true_box = [center_x, center_y, object['xmax'] - object['xmin'], object['ymax'] - object['ymin']]
                truth_batch[instance_count, 0, 0, 0, true_box_index] = true_box

                true_box_index += 1
                true_box_index = true_box_index % self.max_box_per_image

            if self.norm is not None:
                # Assign input image to x_batch
                x_batch[instance_count] = self.norm(image)
            else:
                # Plot image and bounding boxes for sanity check
                for object in all_objects:
                    cv2.rectangle(image, (object['xmin'], object['ymin']), (object['xmax'], object['ymax']), (255, 0, 0), 3)
                    cv2.putText(image, object['name'],
                                (object['xmin'] + 2, object['ymin'] + 12),
                                0, 1.2e-3 * image.shape[0],
                                (0, 255, 0), 2)

                x_batch[instance_count] = image

            # Increase instance counter in the current batch
            instance_count += 1

        # First list is used for the training model, second for the inference model
        # return [x_batch, truth_batch, yolo_1, yolo_2, yolo_3, yolo_4], [dummy_yolo_1, dummy_yolo_2, dummy_yolo_3, dummy_yolo_4]
        return [x_batch, truth_batch, yolo_1, yolo_2, yolo_3], [dummy_yolo_1, dummy_yolo_2, dummy_yolo_3]

    def _get_net_size(self, index):
        """
        Get network input size
        Change the size every 10
        :param index: Index of image
        :return: Network height and width
        """
        if index % 10 == 0:
            net_size = self.downsample * np.random.randint(self.min_net_size // self.downsample,
                                                           self.max_net_size // self.downsample + 1)
            print("Resizing to: ", net_size, net_size)
            self.net_h, self.net_w = net_size, net_size
        return self.net_h, self.net_w

    def _aug_image(self, image_details, net_h, net_w):
        """
        Augment image by applying preprocessing techniques like
        1) Scaling
        2) Flipping
        :param image_details: Image details, filename etc
        :param net_h: Network height
        :param net_w: Network width
        :return: Augmented image
        """
        image_name = image_details['filename']
        image = np.load(image_name)
        # image = cv2.imread(image_name)  # RGB image

        # Sometimes the image cannot be found (Not sure why)
        if image is None:
            print('Cannot find ', image_name)
        # image = image[:, :, ::-1]  # cv2 loads image as BGR, this reverses it to RGB

        image_h, image_w, _ = image.shape

        # Determine the amount of scaling and cropping
        dw = self.jitter * image_w
        dh = self.jitter * image_h

        new_ar = (image_w + np.random.uniform(-dw, dw)) / (image_h + np.random.uniform(-dh, dh))

        # Scale amount
        scale = np.random.uniform(0.25, 2)

        if new_ar < 1:
            new_h = int(scale * net_h)
            new_w = int(net_h * new_ar)
        else:
            new_w = int(scale * net_w)
            new_h = int(net_w / new_ar)

        dx = int(np.random.uniform(0, net_w - new_w))
        dy = int(np.random.uniform(0, net_h - new_h))

        # apply scaling and cropping
        image = apply_random_scale_and_crop(image, new_w, new_h, net_w, net_h, dx, dy)

        # randomly distort hsv space
        # image = random_distort_image(image)

        # Randomly flip
        flip = np.random.randint(2)
        image = random_flip(image, flip)

        # Correct the size and position of bounding boxes
        all_objs = correct_bounding_boxes(image_details['object'], new_w, new_h, net_w, net_h, dx, dy, flip, image_w, image_h)

        return image, all_objs

    def size(self):
        return len(self.images)

    def on_epoch_end(self):
        """
        At the end of one epoch, reshuffle
        :return: None
        """
        if self.shuffle:
            np.random.shuffle(self.images)

    def num_classes(self):
        """
        Get number of classes
        :return: Number of classes
        """
        return len(self.labels)

    def get_anchors(self):
        """
        Get anchors
        :return: List of anchor tuples
        """
        anchors = []

        for anchor in self.anchors:
            anchors += [anchor.xmax, anchor.ymax]

        return anchors

    def load_annotation(self, index):
        """
        Load annotations for image at index
        :param index: Index of image
        :return: List of annotations for image
        """
        annotations = []

        for obj in self.images[index]['object']:
            annotation = [obj['xmin'], obj['ymin'], obj['xmax'], obj['ymax'], self.labels.index(obj['name'])]
            annotations += [annotation]

        if len(annotations) == 0:
            annotations = [[]]

        return np.array(annotations)

    def load_image(self, index):
        """
        Load image at given index and apply normalization if given
        :param index: Index of image in self.images
        :return: Loaded image
        """
        image = cv2.imread(self.images[index]['filename'])

        # Normalize if function has been provided
        if self.norm:
            image = self.norm(image)

        return image
