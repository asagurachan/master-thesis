"""
University of Copenhagen
Master Thesis

Deep Learning Pipeline For Iceberg Detection

Darrick Joo Jian Wen (NGC655)
"""
import numpy as np
from PIL import Image
from imgaug import augmenters as iaa
from keras.utils import Sequence


class BatchGenerator(Sequence):
    def __init__(self,
                 images,
                 config,
                 shuffle=True,
                 jitter=True,
                 labels=True,
                 norm=None,
                 evalu=False):
        self.generator = None

        self.images = images
        self.config = config
        self.shuffle = shuffle
        self.jitter = jitter
        self.norm = norm
        self.labels = labels
        self.evalu = evalu
        self.band = True if config["model"]["channel_size"] == 13 else False
        self.batch_size = self.config["train"]["batch_size"]
        self.input_size = self.config["model"]["input_size"]
        self.channel_size = self.config["model"]["channel_size"]

        self.aug_techniques = []
        self.create_aug_pipeline()

        self.aug_pipeline = iaa.Sequential(self.aug_techniques, random_order=True)

        if self.shuffle:
            np.random.shuffle(self.images)

    def create_aug_pipeline(self):
        """
        "horizontal_flips": true,
        "vertical_flips": true,
        "rotation_range": 360,
        "fill_mode": "constant",
        "zoom_range": 0.10,
        """

        horizontal_flips = self.config["train"]["horizontal_flips"]
        vertical_flips = self.config["train"]["vertical_flips"]
        rotation_range = self.config["train"]["rotation_range"]
        zoom_range = self.config["train"]["zoom_range"]
        shear_range = self.config["train"]["shear_range"]

        sometimes = lambda aug: iaa.Sometimes(0.5, aug)

        # Flips
        if horizontal_flips:
            self.aug_techniques.append(iaa.Fliplr(0.5))
        if vertical_flips:
            self.aug_techniques.append(iaa.Flipud(0.5))

        # Affine
        self.aug_techniques.append(
            sometimes(iaa.Affine(
                scale=(1 - zoom_range, 1 + zoom_range)
            )))
        self.aug_techniques.append(
            sometimes(iaa.Affine(
                rotate=(0, rotation_range)
            )))
        self.aug_techniques.append(
            sometimes(iaa.Affine(
                shear=(-shear_range, shear_range)
            )))

        # self.aug_techniques.append(
        #     iaa.SomeOf((0, 5),
        #                [
        #                    iaa.GaussianBlur((0, 3.0)),
        #                    # iaa.OneOf([
        #                    #     iaa.GaussianBlur((0, 3.0)),
        #                    #     iaa.AverageBlur(k=(2, 7)),
        #                    #     iaa.MedianBlur(k=(3, 11)),
        #                    # ]),
        #                    iaa.Sharpen(alpha=(0.0, 1.0),
        #                                lightness=(0.75, 1.5)),
        #                    sometimes(iaa.OneOf([
        #                        iaa.EdgeDetect(alpha=(0, 0.7)),
        #                        iaa.DirectedEdgeDetect(alpha=(0, 0.7), direction=(0.0, 1.0)),
        #                    ])),
        #                    iaa.AdditiveGaussianNoise(loc=0, scale=(0.0, 0.05 * 255), per_channel=0.5),
        #                    iaa.Dropout((0.01, 0.1), per_channel=0.5),
        #                    iaa.Add((-10, 10), per_channel=0.5),
        #                    # change brightness of images (by -10 to 10 of original value)
        #                    iaa.Multiply((0.5, 1.5), per_channel=0.5),
        #                    # change brightness of images (50-150% of original value)
        #                    iaa.ContrastNormalization((0.5, 2.0), per_channel=0.5),  # improve or worsen the contrast
        #                ], random_order=True)
        # )

    def __len__(self):
        return int(np.ceil(float(len(self.images)) / self.batch_size))

    def __getitem__(self, index):
        l_bound = index * self.batch_size
        r_bound = (index + 1) * self.batch_size

        if r_bound > len(self.images):
            r_bound = len(self.images)
            if not self.evalu:
                l_bound = r_bound - self.batch_size

        instance_count = 0

        x_batch = np.zeros((r_bound - l_bound, self.input_size, self.input_size, self.channel_size))
        y_batch = np.zeros((r_bound - l_bound, len(self.config['model']['labels'])))

        for train_instance in self.images[l_bound:r_bound]:
            img = self.aug_image(train_instance, jitter=self.jitter)

            if self.norm is not None:
                x_batch[instance_count] = self.norm(img)
            else:
                x_batch[instance_count] = img
            y_batch[instance_count, train_instance['label']] = 1

            instance_count += 1

        if self.labels:
            return [x_batch, y_batch]
        else:
            return x_batch

    def get_item(self, index):
        if index > len(self.images):
            raise Exception("Index out of bounds")

        train_instance = self.images[index]
        img = self.aug_image(train_instance, jitter=self.jitter)

        if self.labels:
            return [img, train_instance['label']]
        else:
            return img

    def get_item_data(self, index):
        if index > len(self.images):
            raise Exception("Index out of bounds")

        return self.images[index]

    def on_epoch_end(self):
        if self.shuffle:
            np.random.shuffle(self.images)

    def aug_image(self, train_instance, jitter):
        image_path = train_instance['filepath']
        if self.band:
            image = np.load(image_path)
        else:
            image = np.array(Image.open(image_path))

        if image is None:
            print("Could not find {}".format(image_path))

        if self.band:
            if np.isnan(np.sum(image)):
                print("Image {} has nan value".format(image_path))

        h, w, c = image.shape

        if jitter:
            image = self.aug_pipeline.augment_image(image)

        center = int(h / 2)
        input_size = int(self.input_size / 2)

        lrc = center - input_size
        brc = center + input_size

        image = image[lrc:brc, lrc:brc]

        return image
