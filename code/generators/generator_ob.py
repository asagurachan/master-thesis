"""
University of Copenhagen
Master Thesis

Deep Learning Pipeline For Iceberg Detection

Darrick Joo Jian Wen (NGC655)
"""

import copy

import cv2
import numpy as np
from imgaug import augmenters as iaa
from keras.utils import Sequence

from extras.nn_util import BoundBox, bbox_iou


class BatchGenerator(Sequence):
    def __init__(self,
                 images,
                 config,
                 shuffle=True,
                 jitter=True,
                 norm=None):
        self.generator = None

        self.images = images
        self.labels = config['LABELS']
        self.num_classes = config['CLASS']
        self.box = config['BOX']
        self.true_box_buffer = config['TRUE_BOX_BUFFER']
        self.anchors = config['ANCHORS']
        self.batch_size = config['BATCH_SIZE']
        self.grid_h = config['GRID_H']
        self.grid_w = config['GRID_W']
        self.image_h = config['IMAGE_H']
        self.image_w = config['IMAGE_W']
        self.channels = config['IMAGE_C']

        self.shuffle = shuffle
        self.jitter = jitter
        self.norm = norm

        self.anchors = [BoundBox(0, 0, self.anchors[2 * i], self.anchors[2 * i + 1]) for i in
                        range(int(len(self.anchors) // 2))]

        self.aug_techniques = []
        # self.create_aug_pipeline()

        self.aug_pipe = iaa.Sequential(
            self.aug_techniques,
            random_order=True
        )

        if shuffle:
            np.random.shuffle(self.images)

    def create_aug_pipeline(self):
        self.aug_techniques.append(iaa.SomeOf((0, 5),
                                              [
                                                  iaa.OneOf([
                                                      # blur images with a sigma between 0 and 3.0
                                                      iaa.GaussianBlur((0, 3.0)),
                                                      # blur image using local means with kernel sizes between 2 and 7
                                                      iaa.AverageBlur(k=(2, 7)),
                                                      # blur image using local medians with kernel sizes between 2 and 7
                                                      iaa.MedianBlur(k=(3, 11)),
                                                  ]),
                                                  iaa.Sharpen(alpha=(0, 1.0), lightness=(0.75, 1.5)),  # sharpen images
                                                  iaa.Emboss(alpha=(0, 1.0), strength=(0, 2.0)),  # emboss images
                                                  iaa.AdditiveGaussianNoise(loc=0, scale=(0.0, 0.05 * 255),
                                                                            per_channel=0.5),
                                                  # add gaussian noise to images
                                                  iaa.Dropout((0.01, 0.1), per_channel=0.5),
                                                  iaa.Add((-10, 10), per_channel=0.5),
                                                  # change brightness of images (by -10 to 10 of original value)
                                                  iaa.Multiply((0.5, 1.5), per_channel=0.5),
                                                  # change brightness of images (50-150% of original value)
                                                  iaa.ContrastNormalization((0.5, 2.0), per_channel=0.5)
                                                  # improve or worsen the contrast
                                              ],
                                              random_order=True))

    @staticmethod
    def sometimes(aug):
        return iaa.Sometimes(0.5, aug)

    def get_length(self):
        return len(self.images)

    def __len__(self):
        return int(np.ceil(float(len(self.images)) / self.batch_size))

    def __getitem__(self, idx):
        l_bound = idx * self.batch_size
        r_bound = (idx + 1) * self.batch_size

        if r_bound > len(self.images):
            r_bound = len(self.images)
            l_bound = r_bound - self.batch_size

        instance_count = 0

        x_batch = np.zeros((r_bound - l_bound, self.image_h, self.image_w, self.channels))  # input images
        b_batch = np.zeros((r_bound - l_bound, 1, 1, 1, self.true_box_buffer,
                            4))  # list of self.config['TRUE_self.config['BOX']_BUFFER'] GT boxes
        y_batch = np.zeros((r_bound - l_bound, self.grid_h, self.grid_w, self.box,
                            4 + 1 + self.num_classes))  # desired network output

        for train_instance in self.images[l_bound:r_bound]:
            # augment input image and fix object's position and size
            img, all_objs = self.aug_image(train_instance, jitter=self.jitter)

            # construct output from object's x, y, w, h
            true_box_index = 0

            for obj in all_objs:
                if obj['xmax'] > obj['xmin'] and obj['ymax'] > obj['ymin'] and obj['name'] in self.labels:
                    center_x = .5 * (obj['xmin'] + obj['xmax'])
                    center_x = center_x / (float(self.image_w) / self.grid_w)
                    center_y = .5 * (obj['ymin'] + obj['ymax'])
                    center_y = center_y / (float(self.image_h) / self.grid_h)

                    grid_x = int(np.floor(center_x))
                    grid_y = int(np.floor(center_y))

                    if grid_x < self.grid_w and grid_y < self.grid_h:
                        obj_indx = self.labels.index(obj['name'])

                        # unit: grid cell
                        center_w = (obj['xmax'] - obj['xmin']) / (float(self.image_w) / self.grid_w)
                        center_h = (obj['ymax'] - obj['ymin']) / (float(self.image_h) / self.grid_h)

                        box = [center_x, center_y, center_w, center_h]

                        # find the anchor that best predicts this box
                        best_anchor = -1
                        max_iou = -1

                        shifted_box = BoundBox(0,
                                               0,
                                               center_w,
                                               center_h)

                        for i in range(len(self.anchors)):
                            anchor = self.anchors[i]
                            iou = bbox_iou(shifted_box, anchor)

                            if max_iou < iou:
                                best_anchor = i
                                max_iou = iou

                        # assign ground truth x, y, w, h, confidence and class probs to y_batch
                        y_batch[instance_count, grid_y, grid_x, best_anchor, 0:4] = box
                        y_batch[instance_count, grid_y, grid_x, best_anchor, 4] = 1.
                        y_batch[instance_count, grid_y, grid_x, best_anchor, 5 + obj_indx] = 1

                        # assign the true box to b_batch
                        b_batch[instance_count, 0, 0, 0, true_box_index] = box

                        true_box_index += 1
                        true_box_index = true_box_index % self.true_box_buffer

            # assign input image to x_batch
            if self.norm is not None:
                x_batch[instance_count] = self.norm(img)
            else:
                # plot image and bounding boxes for sanity check
                for obj in all_objs:
                    if obj['xmax'] > obj['xmin'] and obj['ymax'] > obj['ymin']:
                        cv2.rectangle(img[:, :, ::-1], (obj['xmin'], obj['ymin']), (obj['xmax'], obj['ymax']),
                                      (255, 0, 0), 3)
                        cv2.putText(img[:, :, ::-1], obj['name'],
                                    (obj['xmin'] + 2, obj['ymin'] + 12),
                                    0, 1.2e-3 * img.shape[0],
                                    (0, 255, 0), 2)

                x_batch[instance_count] = img

            # increase instance counter in current batch
            instance_count += 1

            # print ' new batch created', idx

        return [x_batch, b_batch], y_batch

    def on_epoch_end(self):
        if self.shuffle:
            np.random.shuffle(self.images)

    def aug_image(self, train_instance, jitter):
        image_name = train_instance['filename']
        band = True if image_name[-3:] == 'npy' else False
        if band:
            image = np.load(image_name)
        else:
            image = cv2.imread(image_name)

        if image is None:
            print('Cannot find ', image_name)

        h, w, c = image.shape
        if w == 0:
            print(train_instance)
        # print("{} {} {}".format(h,w,c))
        all_objs = copy.deepcopy(train_instance['object'])
        offx = 0
        offy = 0
        flip = 0

        if jitter and not band:
            # scale the image
            scale = np.random.uniform() / 10. + 1.
            image = cv2.resize(image, (0, 0), fx=scale, fy=scale)

            # translate the image
            max_offx = (scale - 1.) * w
            max_offy = (scale - 1.) * h
            offx = int(np.random.uniform() * max_offx)
            offy = int(np.random.uniform() * max_offy)

            image = image[offy: (offy + h), offx: (offx + w)]

            # flip the image
            flip = np.random.binomial(1, .5)
            if flip > 0.5:
                image = cv2.flip(image, 1)

            image = self.aug_pipe.augment_image(image)

            # resize the image to standard size
        if not band:
            image = cv2.resize(image, (self.image_h, self.image_w))
            image = image[:, :, ::-1]

        # fix object's position and size
        if not band:
            for obj in all_objs:
                for attr in ['xmin', 'xmax']:
                    if jitter:
                        obj[attr] = int(obj[attr] * scale - offx)

                    obj[attr] = int(obj[attr] * float(self.image_w) / w)
                    obj[attr] = max(min(obj[attr], self.image_w), 0)

                for attr in ['ymin', 'ymax']:
                    if jitter:
                        obj[attr] = int(obj[attr] * scale - offy)

                    obj[attr] = int(obj[attr] * float(self.image_h) / h)
                    obj[attr] = max(min(obj[attr], self.image_h), 0)

                if jitter and flip > 0.5:
                    xmin = obj['xmin']
                    obj['xmin'] = self.image_w - obj['xmax']
                    obj['xmax'] = self.image_w - xmin

        if len(image[np.isnan(image)]) > 0:
            print("bad")

        return image, all_objs
