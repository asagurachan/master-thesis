"""
University of Copenhagen
Master Thesis

Deep Learning Pipeline For Iceberg Detection

Darrick Joo Jian Wen (NGC655)
"""

import argparse
import json
import os
import predict
import train
import evaluate as eva
from keras import backend as K
from extras import util
from tensorflow.python import debug as tf_debug


def main(args):
    """
    Checks arguments given by user
    Load config
    Create missing folders
    Starts the appropriate functions
    """
    config_path = args.conf
    device_id = args.device
    image_path = args.image
    patch = args.patch
    gen = args.gen
    object_detection = args.objd
    evaluate = args.evaluate
    yolov3 = args.yv3
    raccoon = args.rac

    debug = False

    config = None

    # Load config file
    # If invalid, do not proceed
    try:
        with open(config_path) as config_buffer:
            config = json.loads(config_buffer.read())
    except TypeError:
        print("Config file missing")

    # Check and create missing folders for saving
    util.check_create_missing_folders()

    # Specify GPU usage
    if device_id is not -1:
        os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"
        os.environ["CUDA_VISIBLE_DEVICES"] = device_id
        print("Running on GPU device id {}".format(device_id))
    else:
        print("Running on all GPUs")

    # sess = K.get_session()
    # if debug:
    #     sess = tf_debug.LocalCLIDebugWrapperSession(sess)
    #     K.set_session(sess)

    with K.get_session():
        if image_path is not None:
            # Predicting image
            if patch:
                # If the image is a patch
                predict.predict_patch(config, image_path)
            else:
                if object_detection:
                    if yolov3:
                        if evaluate:
                            eva.evaluate_image(config, image_path)
                        elif image_path[-4] == ".":
                            predict.predict_ob_v3_image(config, image_path)
                        else:
                            predict.predict_gen_ob_v3_folder(config, image_path)
                    else:
                        predict.predict_gen_ob_folder(config, image_path)
                else:
                    # Sliding window approach on a whole image
                    predict.predict_sliding_window(config, image_path)
        else:
            if gen:
                # Prediction on generators of dataset
                if object_detection:
                    predict.predict_gen_ob(config)
                else:
                    predict.predict_gen(config)
            else:
                # Train model
                if object_detection:
                    if evaluate:
                        eva.evaluate_generators(config)
                    else:
                        if yolov3:
                            if raccoon:
                                train.train_via_object_detection_v3_rac(config)
                            else:
                                train.train_via_object_detection_v3(config)
                        else:
                            train.train_via_object_detection(config)
                else:
                    train.train(config)


if __name__ == '__main__':
    """
    Valid arguments
    
    conf -c      | Config file
    device -d    | GPU device ids
    patch -p     | If image for prediction is a patch
    image -i     | Image for prediction
    evaluate - e | Evaluate model
    """
    argparser = argparse.ArgumentParser()
    argparser.add_argument('-c',
                           '--conf',
                           default='./configs/config.json',
                           help='Path to config file. Default=./config.json')
    argparser.add_argument('-d',
                           '--device',
                           default="0",
                           help='CUDA device ID. Default=0, -1 to use all GPUs, 0,1,2 to use select multiple GPUs')
    argparser.add_argument('-i',
                           '--image',
                           default=None,
                           help='Path to image for object detection. Default=None')
    argparser.add_argument('-p',
                           '--patch',
                           action="store_const",
                           const=True,
                           default=False,
                           help='Used with -i, True when the image used is a patch and not the full image. Default=False')
    argparser.add_argument('-g',
                           '--gen',
                           action="store_const",
                           const=True,
                           default=False)
    argparser.add_argument('-o',
                           '--objd',
                           action="store_const",
                           const=True,
                           default=False,
                           help="To train via object detection")
    argparser.add_argument('-e',
                           '--evaluate',
                           action="store_const",
                           const=True,
                           default=False,
                           help="Evaluate model")
    argparser.add_argument('-3',
                           '--yv3',
                           action="store_const",
                           const=True,
                           default=False,
                           help="Yolov3")
    argparser.add_argument('-r',
                           '--rac',
                           action="store_const",
                           const=True,
                           default=False,
                           help="Raccoon")

    main(argparser.parse_args())
