"""
University of Copenhagen
Master Thesis

Deep Learning Pipeline For Iceberg Detection

Darrick Joo Jian Wen (NGC655)
"""
from keras.callbacks import CSVLogger, EarlyStopping, TensorBoard, ModelCheckpoint, ReduceLROnPlateau
import datetime


class BaseNet:
    def __init__(self, config):
        self.class_name = None

        model = config["model"]
        train = config["train"]
        validation = config["validation"]

        self.input_size = model.get("input_size")
        self.weights_folder = model.get("weights_folder")
        self.load_weight = model.get("load_weights")
        self.channel_size = model.get("channel_size")
        self.classes = model.get("labels")
        self.input_shape = (self.input_size, self.input_size, self.channel_size)
        self.output_size = len(self.classes)
        self.early_stopping_patience = train.get("early_stopping_patience")
        self.early_stopping_delta = train.get("early_stopping_delta")
        self.verbose = model.get('verbose')
        self.continue_training = train.get('continue_training')

        self.learning_rate = train.get("learning_rate")
        self.basic_loss = train.get("loss")
        self.training_image_folder = train.get("images_folder")
        self.validation_image_folder = validation.get("images_folder")
        self.input_size = model.get("input_size")
        self.epochs = train.get("epochs")
        self.training_size = train.get("training_size")
        self.batch_size = train.get("batch_size")
        self.steps_per_epoch = self.training_size / self.batch_size
        self.log = train.get("log")
        self.weights_folder = model.get("weights_folder")
        # self.ignore_thresh = model.get("ignore_thresh")
        self.max_input_size = model.get('max_input_size')
        self.anchors = model.get("anchors")

        self.object_thresh = model.get("object_thresh")
        self.nms_thresh = model.get("nms_thresh")
        self.grid_scales = model.get("grid_scales")
        self.object_scale = model.get("object_scale")
        self.no_object_scale = model.get("no_object_scale")
        self.xywh_scale = model.get("xywh_scale")
        self.class_scale = model.get("class_scale")

        self.logger = None
        self.tensorboard = None
        self.checkpoint = None
        self.early_stop = None
        self.reduce_on_plateau = None
        self.feature_extractor = None
        self.feature_extractor_infer = None

    def train(self, train_gen, validation_gen, test_gen):
        self.logger = CSVLogger('logs/{}_log.csv'.format(self.class_name), append=True, separator=';')

        self.tensorboard = TensorBoard(log_dir='logs/basic_{}'.format(str(datetime.datetime.
                                                                          now().strftime("%Y-%m-%d %H-%M-%S"))),
                                       histogram_freq=0,
                                       # write_batch_performance=True,
                                       write_graph=True,
                                       write_images=False)
        filename = "{}{}".format(self.weights_folder, self.class_name)
        filename += "-{epoch:04d}.h5"
        self.checkpoint = ModelCheckpoint(filename,
                                          monitor='val_loss',
                                          verbose=self.verbose,
                                          save_best_only=False,
                                          save_weights_only=True,
                                          mode='min',
                                          period=5)
        self.early_stop = EarlyStopping(monitor='val_loss',
                                        min_delta=self.early_stopping_delta,
                                        patience=self.early_stopping_patience,
                                        mode='min',
                                        verbose=self.verbose)
        self.reduce_on_plateau = ReduceLROnPlateau(monitor='val_loss',
                                                   factor=0.5,
                                                   patience=10,
                                                   verbose=self.verbose,
                                                   mode='min',
                                                   epsilon=0.00001,
                                                   cooldown=0,
                                                   min_lr=0)

    def create_model(self):
        raise NotImplementedError("To be implemented in subclass")

    def predict(self, image):
        raise NotImplementedError("To be implemented in subclass")

    def predict_on_batch(self, images):
        raise NotImplementedError("To be implemented in subclass")

    def load_weights(self):
        print("Loading weights")
        self.feature_extractor.load_weights("{}{}.h5".format(self.weights_folder, self.class_name))

    def save_weights(self):
        print("Savings weights")
        self.feature_extractor.save_weights("{}{}.h5".format(self.weights_folder, self.class_name))

    def normalize(self, image):
        raise NotImplementedError("To be implemented in subclass")

    def loss(self, y_true, y_pred):
        return self.basic_loss