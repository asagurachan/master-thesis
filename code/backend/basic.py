"""
University of Copenhagen
Master Thesis

Deep Learning Pipeline For Iceberg Detection

Darrick Joo Jian Wen (NGC655)
"""

from keras.layers import Input, Conv2D, MaxPooling2D, Flatten, Dropout, Dense, LeakyReLU
from keras import Model
from backend.baseNet import BaseNet


class BasicNetFeature(BaseNet):
    def __init__(self, config):
        BaseNet.__init__(self, config)
        self.class_name = self.__class__.__name__
        self.input_image = Input(shape=self.input_shape)

        self.feature_extractor = self.create_model()

        if self.continue_training:
            self.load_weights()

    def train(self, train_gen, validation_gen, test_gen):
        print("Running Basic training")
        BaseNet.train(self, train_gen, validation_gen, test_gen)

        callbacks = [self.tensorboard, self.checkpoint, self.early_stop]
        if self.log:
            callbacks.append(self.logger)

        self.feature_extractor.compile(loss=self.basic_loss,
                                       optimizer='adam',
                                       metrics=['accuracy'])

        self.feature_extractor.fit_generator(generator=train_gen,
                                             validation_data=validation_gen,
                                             steps_per_epoch=self.steps_per_epoch,
                                             epochs=self.epochs,
                                             callbacks=callbacks,
                                             verbose=self.verbose)

        evaluation = self.feature_extractor.evaluate_generator(generator=test_gen,
                                                               steps=len(test_gen))

        print("Test evaluation: {}".format(evaluation))
        print("Training completed")

    def create_model(self):
        print("Creating model")
        # x = Conv2D(32,
        #            kernel_size=(3, 3),
        #            activation='relu')(self.input_image)
        # x = Conv2D(64,
        #            kernel_size=(3, 3),
        #            activation='relu')(x)
        # x = MaxPooling2D(pool_size=(2, 2))(x)
        # x = Dropout(0.25)(x)
        # x = Flatten()(x)
        # x = Dense(128,
        #           activation='relu')(x)
        # x = Dropout(0.5)(x)
        # x = Dense(self.output_size,
        #           activation='softmax')(x)

        x = Conv2D(32,
                   kernel_size=(3, 3))(self.input_image)
        # x = BatchNormalization()(x)
        x = LeakyReLU(alpha=0.1)(x)
        x = Conv2D(64,
                   kernel_size=(3, 3))(x)
        # x = BatchNormalization()(x)
        x = LeakyReLU(alpha=0.1)(x)
        x = Conv2D(128,
                   kernel_size=(3, 3))(x)
        # x = BatchNormalization()(x)
        x = LeakyReLU(alpha=0.1)(x)
        x = MaxPooling2D(pool_size=(2, 2))(x)
        x = Dropout(0.25)(x)
        x = Flatten()(x)
        x = Dense(128)(x)
        x = LeakyReLU(alpha=0.1)(x)
        x = Dropout(0.5)(x)
        x = Dense(self.output_size,
                  activation='softmax')(x)

        model = Model(self.input_image, x)
        model.summary()

        return model

    def predict(self, image):
        image = self.normalize(image)

        return self.feature_extractor.predict(image, verbose=0)

    def normalize(self, image):
        return image / 255.
