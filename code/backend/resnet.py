"""
University of Copenhagen
Master Thesis

Deep Learning Pipeline For Iceberg Detection

Darrick Joo Jian Wen (NGC655)
"""

from keras.layers import Flatten, Dense
from keras import Model
from keras.applications.resnet50 import ResNet50
from backend.baseNet import BaseNet


class ResnetNetFeature(BaseNet):
    def __init__(self, config):
        BaseNet.__init__(self, config)
        self.class_name = self.__class__.__name__

        self.feature_extractor = self.create_model()

        if self.continue_training:
            self.load_weights()

    def train(self, train_gen, validation_gen, test_gen):
        print("Running Resnet training")
        BaseNet.train(self, train_gen, validation_gen, test_gen)

        callbacks = [self.tensorboard, self.early_stop, self.checkpoint]
        if self.log:
            callbacks.append(self.logger)

        self.feature_extractor.compile(loss=self.basic_loss,
                                       optimizer='adam',
                                       metrics=['accuracy'])

        self.feature_extractor.fit_generator(generator=train_gen,
                                             validation_data=validation_gen,
                                             steps_per_epoch=self.steps_per_epoch,
                                             epochs=self.epochs,
                                             callbacks=callbacks,
                                             verbose=self.verbose)

        evaluation = self.feature_extractor.evaluate_generator(generator=test_gen,
                                                               steps=len(test_gen))

        print("Test evaluation: {}".format(evaluation))
        print("Training completed")

    def create_model(self):
        model = ResNet50(input_shape=self.input_shape,
                         include_top=False,
                         classes=self.output_size)

        x = Flatten()(model.output)
        x = Dense(self.output_size, activation='softmax')(x)

        model = Model(inputs=model.input, outputs=x)
        model.summary()

        return model

    def predict(self, image):
        image = self.normalize(image)

        return self.feature_extractor.predict(image, verbose=0)

    def normalize(self, image):
        return image / 255.
