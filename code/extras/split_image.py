"""
University of Copenhagen
Master Thesis

Deep Learning Pipeline For Iceberg Detection

Darrick Joo Jian Wen (NGC655)
"""
import argparse
import cv2
import os
import math
import numpy as np
from extras import util
from extras.nn_util_v3 import write_annotation_to_file, BoundBox


def main(args):
    source_image = args.source
    destination_folder = args.destination
    image_size = int(args.image_size)
    half_image_size = image_size // 2

    path, file = os.path.split(source_image)

    imgs, _, _ = util.parse_annotation(os.path.join(path, "annotations"), source_image, ["iceberg"])

    image_boxes = None

    for img in imgs:
        _, filename = os.path.split(img['filename'])

        if filename[:-4] == file[:-4]:
            image_boxes = img['object']

    if image_boxes is not None:
        # image = cv2.imread(source_image)

        image = np.zeros((10980, 10980, 3))
        image[:, :, 0] = cv2.imread(os.path.join(path, "B04.jp2"), cv2.IMREAD_UNCHANGED)
        image[:, :, 1] = cv2.imread(os.path.join(path, "B03.jp2"), cv2.IMREAD_UNCHANGED)
        image[:, :, 2] = cv2.imread(os.path.join(path, "B02.jp2"), cv2.IMREAD_UNCHANGED)

        pad = 100
        image = cv2.copyMakeBorder(image, pad, pad, pad, pad, cv2.BORDER_CONSTANT, value=[0, 0, 0])
        i = 0
        for box in [[box['xmin'], box['ymin'], box['xmax'], box['ymax'], 1] for box in image_boxes]:
            mid_x = (box[2] + box[0]) // 2 + image_size
            mid_y = (box[3] + box[1]) // 2 + image_size
            w = (box[2] - box[0]) // 2
            h = (box[3] - box[1]) // 2

            patch = image[mid_y - 50:mid_y + 50, mid_x - 50:mid_x + 50, :]
            # filename = "ib_{}_{}.png".format(mid_x, mid_y)
            filename = "ib_{}_{}.npy".format(mid_x, mid_y)

            if i < 1000:
                # cv2.imwrite(os.path.join(os.path.join(destination_folder, "validation/iceberg"), filename), patch)
                np.save(os.path.join(os.path.join(destination_folder, "validation/iceberg"), filename), patch)
                write_annotation_to_file(os.path.join(destination_folder, "validation/iceberg"), filename, patch,
                                         [[50 - w, 50 - h, 50 + w, 50 + h]], "iceberg")
            else:
                # cv2.imwrite(os.path.join(os.path.join(destination_folder, "training/iceberg"), filename), patch)
                np.save(os.path.join(os.path.join(destination_folder, "training/iceberg"), filename), patch)
                write_annotation_to_file(os.path.join(destination_folder, "training/iceberg"), filename, patch,
                                         [[50 - w, 50 - h, 50 + w, 50 + h]], "iceberg")
            i += 1


if __name__ == '__main__':
    argparser = argparse.ArgumentParser()
    argparser.add_argument('-s',
                           '--source',
                           help='Source image')
    argparser.add_argument('-d',
                           '--destination',
                           help='Destination folder')
    argparser.add_argument('-i',
                           '--image_size',
                           default=100,
                           help='Image patch size')

    main(argparser.parse_args())