"""
University of Copenhagen
Master Thesis

Deep Learning Pipeline For Iceberg Detection

Darrick Joo Jian Wen (NGC655)
"""

import os
import numpy as np
import cv2


def remove(text, prefix, suffix):
    if text.startswith(prefix) and text.endswith(suffix):
        return text[len(prefix):-len(suffix)]
    else:
        return text[len(prefix):]

blank = np.zeros((10980, 10980, 3))
# blank = cv2.imread('data/full_images/s_good.png')

for the_file in os.listdir('predicted/iceberg/good/'):
    if the_file != '.DS_Store':
        coords = np.asarray(remove(the_file, 'ib_', '.png').split("_"), dtype=int)

        # blank[coords[0], coords[1], 2] = 255
        blank[coords[1], coords[0], 2] = 255

cv2.imwrite('data/full_images/s_good.png', blank)

# blank = cv2.imread('data/full_images/s_bad.png')
blank = np.zeros((10980, 10980, 3))

for the_file in os.listdir('predicted/iceberg/bad/'):
    if the_file != '.DS_Store':
        coords = np.asarray(remove(the_file, 'ib_', '.png').split("_"), dtype=int)

        # blank[coords[0], coords[1], 2] = 255
        blank[coords[1], coords[0], 2] = 255

cv2.imwrite('data/full_images/s_bad.png', blank)