"""
University of Copenhagen
Master Thesis

Deep Learning Pipeline For Iceberg Detection

Darrick Joo Jian Wen (NGC655)
"""

import os

good_filenames = os.listdir('predicted/iceberg/good/')
bad_filenames = os.listdir('predicted/iceberg/bad')

count = 0

for file in os.listdir('predicted/iceberg'):
    if file in good_filenames or file in bad_filenames:
        os.unlink('predicted/iceberg/{}'.format(file))
        count += 1

print("Removed {}".format(count))