"""
University of Copenhagen
Master Thesis

Deep Learning Pipeline For Iceberg Detection

Darrick Joo Jian Wen (NGC655)
"""
import argparse
import json
import cv2
from extras import util
from os import path
from extras.nn_util_v3 import write_annotation_to_file, BoundBox


def main(args):
    config_path = args.conf
    to_size = args.size

    config = None
    # Load config file
    # If invalid, do not proceed
    try:
        with open(config_path) as config_buffer:
            config = json.loads(config_buffer.read())
    except TypeError:
        print("Config file missing")

    # parse annotations of the training set #3 and 2 done
    images, _, _ = util.parse_annotation("./data/od_other_2/annotations/",
                                         "./data/od_other_2/",
                                         config['model']['labels'])

    for image_detail in images:
        filename = image_detail['filename']
        print(filename)
        bounding_boxes = image_detail['object']

        image = cv2.imread(filename)

        if image is None:
            print("Unable to open file")
            continue

        _, iby, ibx = path.splitext(path.basename(filename))[0].split('_')

        ibx = int(ibx)
        iby = int(iby)


        # y, x
        boxes = find_boxes(0, 0, to_size, 256, bounding_boxes)
        save_image_and_boxes(image[0:256, 0:256], 'ib_{}_{}'.format(ibx - 112, iby - 112), boxes)

        boxes = find_boxes(223, 0, 479, to_size, bounding_boxes)
        save_image_and_boxes(image[0:256, 223:479], 'ib_{}_{}'.format(ibx + 107, iby - 112), boxes)

        boxes = find_boxes(0, 223, to_size, 479, bounding_boxes)
        save_image_and_boxes(image[223:479, 0:256], 'ib_{}_{}'.format(ibx - 112, iby + 107), boxes)

        boxes = find_boxes(223, 223, 479, 479, bounding_boxes)
        save_image_and_boxes(image[223:479, 223:479], 'ib_{}_{}'.format(ibx + 107, iby + 107), boxes)


def find_boxes(xmin, ymin, xmax, ymax, boxes):
    good_boxes = []

    for box in boxes:
        if box['xmin'] >= xmin and box['xmax'] <= xmax and \
           box['ymin'] >= ymin and box['ymax'] <= ymax:
            c_box = BoundBox(abs(box['xmin'] - xmin), abs(box['ymin'] - ymin), abs(box['xmax'] - xmin), abs(box['ymax'] - ymin))
            good_boxes.append(c_box)

    return good_boxes


def save_image_and_boxes(image, filename, boxes):
    cv2.imwrite('./data/od_other_5/' + filename + '.png', image)
    write_annotation_to_file('./data/od_other_5', filename + '.png', image, boxes, 'iceberg')


if __name__ == '__main__':
    argparser = argparse.ArgumentParser()
    argparser.add_argument('-c',
                           '--conf',
                           default='./configs/config.json',
                           help='Path to config file. Default=./config.json')
    argparser.add_argument('-s',
                           '--size',
                           default=256,
                           help='Resize images to given size. Images that are smaller will be cut into given size.')

    main(argparser.parse_args())