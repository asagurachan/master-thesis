"""
University of Copenhagen
Master Thesis

Deep Learning Pipeline For Iceberg Detection

Darrick Joo Jian Wen (NGC655)
"""

import argparse
import cv2
import os
import math
import numpy as np
from extras import util
from extras.nn_util_v3 import write_annotation_to_file, BoundBox


def main(args):
    source_image = args.source
    destination_folder = args.destination
    image_size = int(args.image_size) // 2

    path, file = os.path.split(source_image)

    imgs, _, _ = util.parse_annotation(os.path.join(path, "annotations"), path, ["iceberg"])

    image_boxes = None

    for img in imgs:
        _, filename = os.path.split(img['filename'])

        if filename[:-3] == file[:-3]:
            image_boxes = img['object']

    if image_boxes is not None:
        # image = cv2.imread(source_image)

        image = np.zeros((10980, 10980, 3))
        image[:, :, 0] = cv2.imread(os.path.join(path, "B04.jp2"), cv2.IMREAD_UNCHANGED)
        image[:, :, 1] = cv2.imread(os.path.join(path, "B03.jp2"), cv2.IMREAD_UNCHANGED)
        image[:, :, 2] = cv2.imread(os.path.join(path, "B02.jp2"), cv2.IMREAD_UNCHANGED)

        image_width, image_height, image_channels = image.shape

        width_sections = int(math.ceil(image_width / image_size))-1
        height_sections = int(math.ceil(image_height / image_size))-1

        padded_image = image

        count = 0
        total = (width_sections - 1) * (height_sections - 1)

        # Validation
        for width_section in range(width_sections - 1):
            lower_width_section = width_section * image_size
            higher_width_section = (width_section + 2) * image_size

            # for height_section in range(height_sections - 1):
            for height_section in range(9 - 1):
                lower_height_section = height_section * image_size
                higher_height_section = (height_section + 2) * image_size

                patch = padded_image[lower_height_section:higher_height_section, lower_width_section:higher_width_section, :]

                filename = "ib_{}_{}.npy".format(lower_width_section, lower_height_section)
                # filename = "ib_{}_{}.png".format(lower_width_section, lower_height_section)

                # if count % 10 < 6:
                #     np.save(os.path.join(os.path.join(destination_folder, "training"), filename), patch)
                #     cv2.imwrite(os.path.join(os.path.join(destination_folder, "training"), filename), patch)
                # else:
                np.save(os.path.join(os.path.join(destination_folder, "validation"), filename), patch)
                # cv2.imwrite(os.path.join(os.path.join(destination_folder, "validation"), filename), patch)

                boxes = []
                boxes = find_boxes(image_boxes, lower_width_section, higher_width_section, lower_height_section, higher_height_section)

                # if count % 10 < 6:
                #     write_annotation_to_file(os.path.join(destination_folder, "training"), filename, patch, boxes, "iceberg")
                # else:
                write_annotation_to_file(os.path.join(destination_folder, "validation"), filename, patch, boxes, "iceberg")

                count += 1
                util.print_progress_bar(count, total, prefix='Progress:', suffix='Complete', length=50)

        # Training
        for width_section in range(width_sections - 1):
            lower_width_section = width_section * image_size
            higher_width_section = (width_section + 2) * image_size

            for height_section in range(10, height_sections - 1):
                lower_height_section = height_section * image_size
                higher_height_section = (height_section + 2) * image_size

                patch = padded_image[lower_height_section:higher_height_section, lower_width_section:higher_width_section, :]

                filename = "ib_{}_{}.npy".format(lower_width_section, lower_height_section)
                # filename = "ib_{}_{}.png".format(lower_width_section, lower_height_section)

                # if count % 10 < 6:
                # np.save(os.path.join(os.path.join(destination_folder, "training"), filename), patch)
                # cv2.imwrite(os.path.join(os.path.join(destination_folder, "training"), filename), patch)
                # else:
                np.save(os.path.join(os.path.join(destination_folder, "training"), filename), patch)
                #     cv2.imwrite(os.path.join(os.path.join(destination_folder, "validation"), filename), patch)

                boxes = []
                boxes = find_boxes(image_boxes, lower_width_section, higher_width_section, lower_height_section, higher_height_section)

                # if count % 10 < 6:
                write_annotation_to_file(os.path.join(destination_folder, "training"), filename, patch, boxes, "iceberg")
                # else:
                #     write_annotation_to_file(os.path.join(destination_folder, "validation"), filename, patch, boxes, "iceberg")

                count += 1
                util.print_progress_bar(count, total, prefix='Progress:', suffix='Complete', length=50)


def find_boxes(boxes, low_width, high_width, low_height, high_height):
    actual_boxes = []

    for box in boxes:
        lw = box['xmin']
        hw = box['xmax']
        lh = box['ymin']
        hh = box['ymax']

        if box['xmin'] < low_width:
            lw = low_width
        if box['xmax'] > high_width:
            hw = high_width
        if box['ymin'] < low_height:
            lh = low_height
        if box['ymax'] > high_height:
            hh = high_height

        width = hw - lw
        height = hh - lh

        if width > 0 and height > 0:
            lw = lw - low_width - 1
            hw = hw - low_width - 1
            lh = lh - low_height - 1
            hh = hh - low_height - 1

            actual_boxes.append(BoundBox(lw, lh, hw, hh))

    return actual_boxes


if __name__ == '__main__':
    argparser = argparse.ArgumentParser()
    argparser.add_argument('-s',
                           '--source',
                           help='Source image')
    argparser.add_argument('-d',
                           '--destination',
                           help='Destination folder')
    argparser.add_argument('-i',
                           '--image_size',
                           default=256,
                           help='Image patch size')

    main(argparser.parse_args())