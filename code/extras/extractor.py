"""
University of Copenhagen
Master Thesis

Deep Learning Pipeline For Iceberg Detection

Darrick Joo Jian Wen (NGC655)
"""

import argparse
import random
from os import walk
from xml.etree.ElementTree import Element, SubElement
import cv2
import numpy as np
from PIL import Image
import os
from extras import util

SIZE = 480
include_border = True


def main(args):
    """
    Extract patches from full image or bands
    """
    rgb = args.rgb
    band = args.band
    extras = args.extra

    if rgb or band:
        load_band = band
        load_non_band = rgb
    else:
        load_band = True
        load_non_band = True

    Image.MAX_IMAGE_PIXELS = None
    # img = cv2.imread("data/full_images/S2A dotting.png")
    img = np.array(Image.open("data/full_images/S2A dotting.png"))
    locs = np.where(img[:, :, 0] > 0)

    # Clear memory (To allow gc)
    img = None
    bad = None
    rgb_big = Image.open("data/full_images/S2A full big.png")

    if load_non_band:
        util.delete_files('data/testing/')

    full_band = None
    if load_band:
        # full_band = np.zeros((11180, 11180, 13))
        full_band = np.zeros((11460, 11460, 13))

        full_band[240:-240, 240:-240, 0] = cv2.imread("data/full_images/V20150710T153226/Resized/B01.tif", cv2.IMREAD_UNCHANGED)
        full_band[240:-240, 240:-240, 1] = cv2.imread("data/full_images/V20150710T153226/Resized/B02.tiff", cv2.IMREAD_UNCHANGED)
        full_band[240:-240, 240:-240, 2] = cv2.imread("data/full_images/V20150710T153226/Resized/B03.tiff", cv2.IMREAD_UNCHANGED)
        full_band[240:-240, 240:-240, 3] = cv2.imread("data/full_images/V20150710T153226/Resized/B04.tiff", cv2.IMREAD_UNCHANGED)
        full_band[240:-240, 240:-240, 4] = cv2.imread("data/full_images/V20150710T153226/Resized/B05.tif", cv2.IMREAD_UNCHANGED)
        full_band[240:-240, 240:-240, 5] = cv2.imread("data/full_images/V20150710T153226/Resized/B06.tif", cv2.IMREAD_UNCHANGED)
        full_band[240:-240, 240:-240, 6] = cv2.imread("data/full_images/V20150710T153226/Resized/B07.tif", cv2.IMREAD_UNCHANGED)
        full_band[240:-240, 240:-240, 7] = cv2.imread("data/full_images/V20150710T153226/Resized/B08.tiff", cv2.IMREAD_UNCHANGED)
        full_band[240:-240, 240:-240, 8] = cv2.imread("data/full_images/V20150710T153226/Resized/B08A.tif", cv2.IMREAD_UNCHANGED)
        full_band[240:-240, 240:-240, 9] = cv2.imread("data/full_images/V20150710T153226/Resized/B09.tif", cv2.IMREAD_UNCHANGED)
        full_band[240:-240, 240:-240, 10] = cv2.imread("data/full_images/V20150710T153226/Resized/B10.tif", cv2.IMREAD_UNCHANGED)
        full_band[240:-240, 240:-240, 11] = cv2.imread("data/full_images/V20150710T153226/Resized/B11.tif", cv2.IMREAD_UNCHANGED)
        full_band[240:-240, 240:-240, 12] = cv2.imread("data/full_images/V20150710T153226/Resized/B12.tif", cv2.IMREAD_UNCHANGED)

        full_band = (full_band / np.max(full_band, axis=(0, 1))) * 255

        util.delete_files('data/testing_b/')

    print("Red count: {}".format(len(locs[0])))
    radius = int(SIZE / 2)
    count = 0

    shuffle = list(range(len(locs[0])))
    random.shuffle(shuffle)
    x_loc = locs[0][shuffle] * 2
    y_loc = locs[1][shuffle] * 2

    height = rgb_big.size[1]
    width = rgb_big.size[0]

    max_count = len(locs[0])
    util.print_progress_bar(0, max_count, prefix='Progress:', suffix='Complete', length=50)
    for c, r in zip(x_loc, y_loc):

        top_left = (r - radius, c - radius)
        bottom_right = (r + radius, c + radius)

        if check_in_boundary(height, width, top_left, bottom_right, radius):
            if load_non_band:
                extract_iceberg_patch(rgb_big, top_left, bottom_right, count, max_count)
            if load_band:
                extract_iceberg_patch(full_band, top_left, bottom_right, count, max_count, True)
            count += 1
        else:
            max_count -= 1

        util.print_progress_bar(count, max_count, prefix='Progress:', suffix='Complete', length=50)

    print("done icebergs")
    o_count = 0

    other = np.array(Image.open("data/full_images/extras_img.png"))
    other_locs = np.where(other[:, :, 0] > 0)

    shuffle = list(range(len(other_locs[0])))
    random.shuffle(shuffle)
    x_loc = other_locs[0][shuffle]
    y_loc = other_locs[1][shuffle]

    max_count = len(other_locs[0])
    util.print_progress_bar(o_count, max_count, prefix='Progress:', suffix='Complete', length=50)
    for c, r in zip(x_loc, y_loc):
        top_left = (r - radius, c - radius)
        bottom_right = (r + radius, c + radius)

        if check_in_boundary(height, width, top_left, bottom_right, radius):
            if load_non_band:
                extract_other_patch(rgb_big, top_left, bottom_right, o_count, max_count)
            if load_band:
                extract_other_patch(full_band, top_left, bottom_right, o_count, max_count, True)
            o_count += 1
        else:
            max_count -= 1

        util.print_progress_bar(o_count, max_count, prefix='Progress:', suffix='Complete', length=50)

    print("Others done")
    other = None
    other_locs = None

    if extras:
        bad = np.array(Image.open("data/full_images/s_bad.png"))
        bad_locs = np.where(bad[:, :, 0] > 0)
        rgb_big = Image.open("data/full_images/S2A full big.png")

        max_count = len(bad_locs[0])

        shuffle = list(range(len(bad_locs[0])))
        random.shuffle(shuffle)
        x_loc = bad_locs[0][shuffle]
        y_loc = bad_locs[1][shuffle]

        o_o_count = 0
        util.print_progress_bar(o_o_count, max_count, prefix='Progress:', suffix='Complete', length=50)
        for c, r in zip(x_loc, y_loc):

            top_left = (r - radius, c - radius)
            bottom_right = (r + radius, c + radius)

            if check_in_boundary(height, width, top_left, bottom_right, radius):
                if load_non_band:
                    extract_other_patch(rgb_big, top_left, bottom_right, o_o_count, max_count)
                if load_band:
                    extract_other_patch(full_band, top_left, bottom_right, o_o_count, max_count, True)
                o_o_count += 1
            else:
                max_count -= 1
            util.print_progress_bar(o_o_count, max_count, prefix='Progress:', suffix='Complete', length=50)

        print("Extra others done: {}".format(o_o_count))
        bad = None
        bad_locs = None

        good = np.array(Image.open("data/full_images/s_good.png"))
        good_locs = np.where(good[:, :, 0] > 0)

        max_count = len(good_locs[0])

        shuffle = list(range(len(good_locs[0])))
        random.shuffle(shuffle)
        x_loc = good_locs[0][shuffle]
        y_loc = good_locs[1][shuffle]

        o_o_count = 0
        util.print_progress_bar(o_o_count, max_count, prefix='Progress:', suffix='Complete', length=50)
        for c, r in zip(x_loc, y_loc):

            top_left = (r - radius, c - radius)
            bottom_right = (r + radius, c + radius)

            if check_in_boundary(height, width, top_left, bottom_right, radius):
                if load_non_band:
                    extract_iceberg_patch(rgb_big, top_left, bottom_right, o_o_count, max_count)
                if load_band:
                    extract_iceberg_patch(full_band, top_left, bottom_right, o_o_count, max_count, True)
                o_o_count += 1
            else:
                max_count -= 1
            util.print_progress_bar(o_o_count, max_count, prefix='Progress:', suffix='Complete', length=50)

        print("Extra iceberg done: {}".format(o_o_count))
    print("Finished")


def check_in_boundary(h, w, tl, br, boundary):
    (left, upper) = tl
    (right, lower) = br

    if include_border:
        return True

    if boundary < left and w - boundary > right and \
        boundary < upper and h - boundary > lower:
        return True
    else:
        return False


def extract_other_patch(img, tl, br, count, max, band=False):
    (left, upper) = tl
    (right, lower) = br
    center_x = int((right + left) / 2.)
    center_y = int((lower + upper) / 2.)

    if band:
        # patch = img[upper+100:lower+100, left+100:right+100]
        patch = img[upper + 240:lower + 240, left + 240:right + 240]

        if count < max * 0.6:
            np.save("data/testing_b/train/other/o_{}_{}".format(center_x, center_y), patch)
        elif count < max * 0.8:
            np.save("data/testing_b/validation/other/o_{}_{}".format(center_x, center_y), patch)
        else:
            np.save("data/testing_b/test/other/o_{}_{}".format(center_x, center_y), patch)
    else:
        patch = img.crop(box=(left, upper, right, lower))
        if count < max * 0.6:
            patch.save("data/testing/train/other/o_{}_{}.png".format(center_x, center_y), "PNG")
        elif count < max * 0.8:
            patch.save("data/testing/validation/other/o_{}_{}.png".format(center_x, center_y), "PNG")
        else:
            patch.save("data/testing/test/other/o_{}_{}.png".format(center_x, center_y), "PNG")


def extract_iceberg_patch(img, tl, br, count, max, band=False):
    (left, upper) = tl
    (right, lower) = br
    center_x = int((right + left) / 2.)
    center_y = int((lower + upper) / 2.)

    if band:
        # patch = img[upper+100:lower+100, left+100:right+100]
        patch = img[upper + 240:lower + 240, left + 240:right + 240]

        if count < max * 0.6:
            np.save("data/testing_b/train/iceberg/ib_{}_{}".format(center_x, center_y), patch)
        elif count < max * 0.8:
            np.save("data/testing_b/validation/iceberg/ib_{}_{}".format(center_x, center_y), patch)
        else:
            np.save("data/testing_b/test/iceberg/ib_{}_{}".format(center_x, center_y), patch)
    else:
        patch = img.crop(box=(left, upper, right, lower))
        if count < max * 0.6:
            patch.save("data/testing/train/iceberg/ib_{}_{}.png".format(center_x, center_y), "PNG")
        elif count < max * 0.8:
            patch.save("data/testing/validation/iceberg/ib_{}_{}.png".format(center_x, center_y), "PNG")
        else:
            patch.save("data/testing/test/iceberg/ib_{}_{}.png".format(center_x, center_y), "PNG")
    patch = None


def create_annotations(small_size, locs, label):
    top = Element('annotation')
    folder = SubElement(top, 'folder')
    folder.text = 'data/full_images'

    filename = SubElement(top, 'filename')
    filename.text = 'S2A full.png'

    size = SubElement(top, 'size')
    width = SubElement(size, 'width')
    width.text = small_size
    height = SubElement(size, 'height')
    height.text = small_size
    depth = SubElement(size, 'depth')
    depth.text = 3

    for (_, _, filenames) in walk('data/detection/images'):
        for filename in filenames:
            if filename != '.DS_Store':
                object = SubElement(top, 'object')
                name = SubElement(object, 'name')
                name.text = label
                bndbox = SubElement(object, 'bndbox')
                xmin = SubElement(bndbox, 'xmin')
                ymin = SubElement(bndbox, 'ymin')
                xmax = SubElement(bndbox, 'xmax')
                ymax = SubElement(bndbox, 'ymax')

                xmin.text = locs[0]
                ymin.text = locs[1]
                xmax.text = locs[0]
                ymax.text = locs[1]


if __name__ == '__main__':
    argparser = argparse.ArgumentParser()
    argparser.add_argument('-r',
                           '--rgb',
                           default=False,
                           action="store_const",
                           const=True,
                           help='Extract only rgb')
    argparser.add_argument('-b',
                           '--band',
                           default=False,
                           action="store_const",
                           const=True,
                           help='Extract only bands')
    argparser.add_argument('-e',
                           '--extra',
                           default=False,
                           action="store_const",
                           const=True,
                           help='Extract extras')

    main(argparser.parse_args())