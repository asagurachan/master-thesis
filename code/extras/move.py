"""
University of Copenhagen
Master Thesis

Deep Learning Pipeline For Iceberg Detection

Darrick Joo Jian Wen (NGC655)
"""
import argparse
import shutil
from os import path


def main(args):
    filename = args.file + '.png'
    source = args.sdir
    dest = args.dir

    # Move file
    shutil.move(path.join(source, filename), path.join(dest, filename))

    # Move annotation
    filename = args.file + '.xml'
    shutil.move(path.join(path.join(source, 'annotations'), filename),
                path.join(path.join(dest, 'annotations'), filename))


if __name__ == '__main__':
    argparser = argparse.ArgumentParser()
    argparser.add_argument(
        '-f',
        '--file',
        help='File to move')
    argparser.add_argument(
        '-sd',
        '--sdir',
        help="Source directory"
    )
    argparser.add_argument(
        '-dd',
        '--dir',
        help='Destination directory'
    )

    main(argparser.parse_args())