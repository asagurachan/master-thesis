"""
University of Copenhagen
Master Thesis

Deep Learning Pipeline For Iceberg Detection

Darrick Joo Jian Wen (NGC655)
"""
import argparse
import xml.etree.ElementTree as ET
from shutil import copyfile, move
from os import path, listdir


def main(args):
    ignore_anno = args.ia

    anno_path = 'data/od/training/annotations'
    anno_files = listdir(anno_path)

    dest_path = 'data/od_band/training/annotations'
    filenames = []

    if not ignore_anno:
        for af in anno_files:
            dest_file_path = path.join(dest_path, af)
            copyfile(path.join(anno_path, af), dest_file_path)

            tree = ET.parse(dest_file_path)
            filename = tree.find('filename')
            filename.text = filename.text[:-3] + 'npy'
            depth = tree.find('size').find('depth')
            depth.text = '12'
            tree.write(dest_file_path)

            filenames.append(filename.text[:-3] + 'npy')

    band_path_train = 'data/testing_b/train/iceberg'
    band_path_test = 'data/testing_b/test/iceberg'
    band_path_validation = 'data/testing_b/validation/iceberg'
    dest_path = 'data/od_band/training'

    bpt_files = listdir(band_path_train)
    bpv_files = listdir(band_path_validation)
    bpte_files = listdir(band_path_test)

    for bf in filenames:
        try:
            if bpt_files.index(bf):
                copyfile(path.join(band_path_train, bf), path.join(dest_path, bf))
        except ValueError:
            pass

        try:
            if bpv_files.index(bf):
                copyfile(path.join(band_path_validation, bf), path.join(dest_path, bf))
        except ValueError:
            pass

        try:
            if bpte_files.index(bf):
                copyfile(path.join(band_path_test, bf), path.join(dest_path, bf))
        except ValueError:
            pass

    anno_path = 'data/od/validation/annotations'
    anno_files = listdir(anno_path)

    dest_path = 'data/od_band/validation/annotations'
    filenames = []

    if not ignore_anno:
        for af in anno_files:
            dest_file_path = path.join(dest_path, af)
            copyfile(path.join(anno_path, af), dest_file_path)

            tree = ET.parse(dest_file_path)
            filename = tree.find('filename')
            filename.text = filename.text[:-3] + 'npy'
            depth = tree.find('size').find('depth')
            depth.text = '12'
            tree.write(dest_file_path)

            filenames.append(filename.text[:-3] + 'npy')

    band_path_train = 'data/testing_b/train/iceberg'
    band_path_test = 'data/testing_b/test/iceberg'
    band_path_validation = 'data/testing_b/validation/iceberg'
    dest_path = 'data/od_band/validation'

    bpt_files = listdir(band_path_train)
    bpv_files = listdir(band_path_validation)
    bpte_files = listdir(band_path_test)

    for bf in filenames:
        try:
            if bpt_files.index(bf):
                copyfile(path.join(band_path_train, bf), path.join(dest_path, bf))
        except ValueError:
            pass

        try:
            if bpv_files.index(bf):
                copyfile(path.join(band_path_validation, bf), path.join(dest_path, bf))
        except ValueError:
            pass

        try:
            if bpte_files.index(bf):
                copyfile(path.join(band_path_test, bf), path.join(dest_path, bf))
        except ValueError:
            pass


if __name__ == '__main__':
    argparser = argparse.ArgumentParser()
    argparser.add_argument(
        '-sd',
        '--sdir',
        help="Source directory"
    )
    argparser.add_argument(
        '-dd',
        '--dir',
        help='Destination directory'
    )
    argparser.add_argument(
        '-ia',
        '--ia',
        help='Ignore annotations'
    )

    main(argparser.parse_args())

