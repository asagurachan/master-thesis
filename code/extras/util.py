"""
University of Copenhagen
Master Thesis

Deep Learning Pipeline For Iceberg Detection

Darrick Joo Jian Wen (NGC655)
"""

import os
import shutil
from backend.basic import BasicNetFeature
from backend.basic_band import BasicBandNetFeature
from backend.resnet import ResnetNetFeature
from backend.yolo import YoloNetFeature
from backend.yolov3 import Yolov3NetFeature
from backend.tiny_yolo import TinyYoloNetFeature
from backend.basic_ob import BasicObNetFeature
import sys
from os import walk, path
import numpy as np
import xml.etree.ElementTree as ET

FOLDERS_INUSE = ["./models",
                 "./logs",
                 "./data/testing/train/iceberg/",
                 "./data/testing/train/other/",
                 "./data/testing/validation/iceberg/",
                 "./data/testing/validation/other/",
                 "./data/testing/test/iceberg/",
                 "./data/testing/test/other/",
                 "./data/testing_b/train/iceberg/",
                 "./data/testing_b/train/other/",
                 "./data/testing_b/validation/iceberg/",
                 "./data/testing_b/validation/other/",
                 "./data/testing_b/test/iceberg/",
                 "./data/testing_b/test/other/",
                 "./data/predicted/iceberg",
                 "./data/predicted_b/iceberg",
                 "./iceberg"]
                 # "./data/predicted/iceberg",
                 # "./data/predicted/other"]


def check_create_missing_folders():
    """
    Check for missing folders and create them.
    """
#   TODO: Allow custom folders because of config.json
    for path in FOLDERS_INUSE:
        os.makedirs(path, exist_ok=True)


def delete_files(path):
    print("Deleting files in path ({}) and sub-directories".format(path))
    for the_file in os.listdir(path):
        file_path = os.path.join(path, the_file)
        try:
            if os.path.isfile(file_path):
                os.unlink(file_path)
            elif os.path.isdir(file_path): shutil.rmtree(file_path)
        except Exception as e:
            print(e)

    check_create_missing_folders()


def load_architecture(config):
    architecture = config["model"]["architecture"]

    print("Loading architecture {}".format(architecture))

    if architecture == "Basic":
        nn = BasicNetFeature(config)
    elif architecture == "Basic_Band":
        nn = BasicBandNetFeature(config)
    elif architecture == "Resnet":
        nn = ResnetNetFeature(config)
    elif architecture == "Yolo":
        nn = YoloNetFeature(config)
    elif architecture == "Yolo_v3":
        nn = Yolov3NetFeature(config)
    elif architecture == "Tiny_Yolo":
        nn = TinyYoloNetFeature(config)
    elif architecture == "Basic Ob":
        nn = BasicObNetFeature(config)
    else:
        raise Exception("Architecture type not found, {}".format(architecture))

    return nn


def is_band_architecture(architecture):
    if architecture.endswith('Band'):
        return True
    return False


def print_progress_bar(iteration, total, prefix='', suffix='', decimals=1, length=100, fill='█'):
    """
    https://stackoverflow.com/questions/3173320/text-progress-bar-in-the-console
    Call in a loop to create terminal progress bar
    @params:
        iteration   - Required  : current iteration (Int)
        total       - Required  : total iterations (Int)
        prefix      - Optional  : prefix string (Str)
        suffix      - Optional  : suffix string (Str)
        decimals    - Optional  : positive number of decimals in percent complete (Int)
        length      - Optional  : character length of bar (Int)
        fill        - Optional  : bar fill character (Str)
    """
    fill = '*'
    percent = ("{0:." + str(decimals) + "f}").format(100 * (iteration / float(total)))
    filled_length = int(length * iteration // total)
    bar = fill * filled_length + '-' * (length - filled_length)
    sys.stdout.write('%s |%s| %s%% %s\r' % (prefix, bar, percent, suffix))
    # Print New Line on Complete
    if iteration == total:
        sys.stdout.write('\n')
    sys.stdout.flush()


def swap_bgr_rgb(image):
    """
    Basically reverse the colour channels
    Only used when cv2 is involved
    :return: reversed image channels
    """
    return image[:, :, ::-1]


def parse_images(f_path, labels):
    count = np.zeros((len(labels)))
    f = []
    for (dir_path, dir_names, file_names) in walk(f_path):
        dir_names.sort()
        for filename in file_names:
            if filename != '.DS_Store':
                img = {'object': []}
                img['filename'] = filename
                img['filepath'] = '{}{}{}'.format(dir_path,
                                                  '/' if not dir_path.endswith('/') else '',
                                                  filename)
                img['label'] = labels.index(path.basename(dir_path))
                count[img['label']] += 1

                f.append(img)

    return f, count


def parse_annotation(ann_dir, img_dir, labels=None):
    all_imgs = []
    seen_labels = {}

    if labels is None:
        labels = []

    max_box_per_image = 0

    for ann in sorted(os.listdir(ann_dir)):
        if ann != ".DS_Store" and ann[0] != ".":
            img = {'object': []}

            tree = ET.parse(os.path.join(ann_dir, ann))

            for elem in tree.iter():
                if 'filename' in elem.tag:
                    img['filename'] = os.path.join(img_dir, elem.text)
                if 'width' in elem.tag:
                    img['width'] = int(elem.text)
                if 'height' in elem.tag:
                    img['height'] = int(elem.text)
                if 'object' in elem.tag or 'part' in elem.tag:
                    obj = {}

                    for attr in list(elem):
                        if 'name' in attr.tag:
                            obj['name'] = attr.text

                            if obj['name'] in seen_labels:
                                seen_labels[obj['name']] += 1
                            else:
                                seen_labels[obj['name']] = 1

                            if len(labels) > 0 and obj['name'] not in labels:
                                break
                            else:
                                img['object'] += [obj]

                        if 'score' in attr.tag and attr.text != "None":
                            obj['score'] = float(attr.text)

                        if 'bndbox' in attr.tag:
                            for dim in list(attr):
                                if 'xmin' in dim.tag:
                                    obj['xmin'] = int(round(float(dim.text))) #- 1
                                if 'ymin' in dim.tag:
                                    obj['ymin'] = int(round(float(dim.text))) #- 1
                                if 'xmax' in dim.tag:
                                    obj['xmax'] = int(round(float(dim.text))) #- 1
                                if 'ymax' in dim.tag:
                                    obj['ymax'] = int(round(float(dim.text))) #- 1

            max_box_per_image = max(max_box_per_image, len(img['object']))

            if len(img['object']) > 0:
                all_imgs += [img]

    return all_imgs, seen_labels, max_box_per_image
