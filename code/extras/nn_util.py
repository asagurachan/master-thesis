"""
University of Copenhagen
Master Thesis

Deep Learning Pipeline For Iceberg Detection

Darrick Joo Jian Wen (NGC655)
"""

import numpy as np
import cv2
# from xml.etree.ElementTree import Element, SubElement
from xml.etree.cElementTree import Element, SubElement, ElementTree


class BoundBox:
    def __init__(self, x, y, w, h, c=None, classes=None):
        self.x = x
        self.y = y
        self.w = w
        self.h = h

        self.x_min = self.x - self.w / 2
        self.x_max = self.x + self.w / 2
        self.y_min = self.y - self.h / 2
        self.y_max = self.y + self.h / 2

        self.area = self.w * self.h

        self.c = c
        self.classes = classes

        self.label = -1
        self.score = -1

    def get_label(self):
        if self.label == -1:
            self.label = np.argmax(self.classes)

        return self.label

    def get_score(self):
        if self.score == -1:
            self.score = self.classes[self.get_label()]

        return self.score


def bbox_iou(box1, box2):
    # x1_min = box1.x - box1.w / 2
    # x1_max = box1.x + box1.w / 2
    # y1_min = box1.y - box1.h / 2
    # y1_max = box1.y + box1.h / 2
    #
    # x2_min = box2.x - box2.w / 2
    # x2_max = box2.x + box2.w / 2
    # y2_min = box2.y - box2.h / 2
    # y2_max = box2.y + box2.h / 2
    #
    # intersect_w = interval_overlap([x1_min, x1_max], [x2_min, x2_max])
    # intersect_h = interval_overlap([y1_min, y1_max], [y2_min, y2_max])

    intersect_w = interval_overlap([box1.x_min, box1.x_max], [box2.x_min, box2.x_max])
    intersect_h = interval_overlap([box1.y_min, box1.y_max], [box2.y_min, box2.y_max])

    intersect = intersect_w * intersect_h

    # union = box1.w * box1.h + box2.w * box2.h - intersect
    union = box1.area + box2.area - intersect

    if union != 0:
        return float(intersect) / union
    else:
        return 0


def interval_overlap(interval_a, interval_b):
    x1, x2 = interval_a
    x3, x4 = interval_b

    overlap = min(x2, x4) - max(x3, x1)
    return max(overlap, 0)
    # x1, x2 = interval_a
    # x3, x4 = interval_b
    #
    # if x3 < x1:
    #     if x4 < x1:
    #         return 0
    #     else:
    #         return min(x2, x4) - x1
    # else:
    #     if x2 < x3:
    #         return 0
    #     else:
    #         return min(x2, x4) - x3


def draw_boxes(image, boxes, labels):
    for box in boxes:
        x_min = int((box.x - box.w / 2) * image.shape[1])
        x_max = int((box.x + box.w / 2) * image.shape[1])
        y_min = int((box.y - box.h / 2) * image.shape[0])
        y_max = int((box.y + box.h / 2) * image.shape[0])

        cv2.rectangle(image, (x_min, y_min), (x_max, y_max), (0, 255, 0), 1)
        # cv2.putText(image,
        #             labels[box.get_label()] + ' ' + str(box.get_score()),
        #             (x_min, y_min - 13),
        #             cv2.FONT_HERSHEY_SIMPLEX,
        #             1e-3 * image.shape[0],
        #             (0, 255, 0), 2)

    return image


def write_annotation_to_file(path, file_name, image, boxes, label):
    h, w, c = image.shape

    annotation = Element('annotation')
    folder = SubElement(annotation, 'folder')
    folder.text = 'od'

    filename = SubElement(annotation, 'filename')
    filename.text = file_name

    size = SubElement(annotation, 'size')
    width = SubElement(size, 'width')
    width.text = str(w)
    height = SubElement(size, 'height')
    height.text = str(h)
    depth = SubElement(size, 'depth')
    depth.text = str(c)

    segmented = SubElement(annotation, 'segmented')
    segmented.text = '0'

    for box in boxes:
        x_min = int((box.x - box.w / 2) * w)
        x_max = int((box.x + box.w / 2) * w)
        y_min = int((box.y - box.h / 2) * h)
        y_max = int((box.y + box.h / 2) * h)

        label_object = SubElement(annotation, 'object')
        name = SubElement(label_object, 'name')
        name.text = label
        pose = SubElement(label_object, 'pose')
        pose.text = 'Unspecified'
        truncated = SubElement(label_object, 'truncated')
        truncated.text = '0'
        occluded = SubElement(label_object, 'occluded')
        occluded.text = '0'
        difficult = SubElement(label_object, 'difficult')
        difficult.text = '0'

        bndbox = SubElement(label_object, 'bndbox')
        xmi = SubElement(bndbox, 'xmin')
        xmi.text = str(x_min)
        ymi = SubElement(bndbox, 'ymin')
        ymi.text = str(y_min)
        xma = SubElement(bndbox, 'xmax')
        xma.text = str(x_max)
        yma = SubElement(bndbox, 'ymax')
        yma.text = str(y_max)

    tree = ElementTree(annotation)
    tree.write("{}/{}/{}{}".format(path, 'annotations', file_name[:-3], 'xml'))


def decode_netout(netout, anchors, num_class, obj_threshold=0.3, nms_threshold=0.3):
    grid_h, grid_w, nb_box = netout.shape[:3]

    boxes = []

    # decode the output by the network
    netout[..., 4] = sigmoid(netout[..., 4])
    netout[..., 5:] = netout[..., 4][..., np.newaxis] * softmax(netout[..., 5:])
    netout[..., 5:] *= netout[..., 5:] > obj_threshold

    for row in range(grid_h):
        for col in range(grid_w):
            for b in range(nb_box):
                # from 4th element onwards are confidence and class classes
                classes = netout[row, col, b, 5:]

                if classes.any():
                    # first 4 elements are x, y, w, and h
                    x, y, w, h = netout[row, col, b, :4]

                    x = (col + sigmoid(x)) / grid_w  # center position, unit: image width
                    y = (row + sigmoid(y)) / grid_h  # center position, unit: image height
                    w = anchors[2 * b + 0] * np.exp(w) / grid_w  # unit: image width
                    h = anchors[2 * b + 1] * np.exp(h) / grid_h  # unit: image height
                    confidence = netout[row, col, b, 4]

                    box = BoundBox(x, y, w, h, confidence, classes)

                    boxes.append(box)

    # suppress non-maximal boxes
    for c in range(num_class):
        sorted_indices = list(reversed(np.argsort([box.classes[c] for box in boxes])))

        for i in range(len(sorted_indices)):
            index_i = sorted_indices[i]

            if boxes[index_i].classes[c] == 0:
                continue
            else:
                for j in range(i + 1, len(sorted_indices)):
                    index_j = sorted_indices[j]

                    if bbox_iou(boxes[index_i], boxes[index_j]) >= nms_threshold:
                        boxes[index_j].classes[c] = 0

    # remove the boxes which are less likely than a obj_threshold
    boxes = [box for box in boxes if box.get_score() > obj_threshold]

    return boxes


def sigmoid(x):
    return 1. / (1. + np.exp(-x))


def softmax(x, axis=-1, t=-100.):
    x = x - np.max(x)

    if np.min(x) < t:
        x = x / np.min(x) * t

    e_x = np.exp(x)

    return e_x / e_x.sum(axis, keepdims=True)