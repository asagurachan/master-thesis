"""
University of Copenhagen
Master Thesis

Deep Learning Pipeline For Iceberg Detection

Darrick Joo Jian Wen (NGC655)
"""
import math
import time
import cv2
import numpy as np
from PIL import Image
from sklearn.metrics import confusion_matrix
import os
import train
from extras.nn_util import draw_boxes, write_annotation_to_file
from extras.nn_util_v3 import write_annotation_to_file as write_annotation_to_file_v3, bbox_iou, draw_boxes as draw_boxes_v3
from extras import util
from keras.optimizers import Adam

# import matplotlib.pyplot as plt


def predict_sliding_window(config, image_path):
    print("Starting sliding window predictions")
    Image.MAX_IMAGE_PIXELS = None

    nn = util.load_architecture(config)
    nn.load_weights()

    nn.feature_extractor.compile(loss=config['train']['loss'],
                                 optimizer='adam',
                                 metrics=['accuracy'])

    input_size = config["model"]["input_size"]
    radius = math.ceil(input_size / 2)

    true_rgb = Image.open(image_path)
    image = None
    w, h = true_rgb.size

    architecture = config["model"]["architecture"]

    if not util.is_band_architecture(architecture):
        image = true_rgb
        util.delete_files('./data/predicted/iceberg')
    elif util.is_band_architecture(architecture):
        image = np.zeros((11180, 11180, 13))

        image[100:-100, 100:-100, 0] = cv2.imread("data/full_images/V20150710T153226/Resized/B01.tif",
                                                      cv2.IMREAD_UNCHANGED)
        image[100:-100, 100:-100, 1] = cv2.imread("data/full_images/V20150710T153226/Resized/B02.tiff",
                                                      cv2.IMREAD_UNCHANGED)
        image[100:-100, 100:-100, 2] = cv2.imread("data/full_images/V20150710T153226/Resized/B03.tiff",
                                                      cv2.IMREAD_UNCHANGED)
        image[100:-100, 100:-100, 3] = cv2.imread("data/full_images/V20150710T153226/Resized/B04.tiff",
                                                      cv2.IMREAD_UNCHANGED)
        image[100:-100, 100:-100, 4] = cv2.imread("data/full_images/V20150710T153226/Resized/B05.tif",
                                                      cv2.IMREAD_UNCHANGED)
        image[100:-100, 100:-100, 5] = cv2.imread("data/full_images/V20150710T153226/Resized/B06.tif",
                                                      cv2.IMREAD_UNCHANGED)
        image[100:-100, 100:-100, 6] = cv2.imread("data/full_images/V20150710T153226/Resized/B07.tif",
                                                      cv2.IMREAD_UNCHANGED)
        image[100:-100, 100:-100, 7] = cv2.imread("data/full_images/V20150710T153226/Resized/B08.tiff",
                                                      cv2.IMREAD_UNCHANGED)
        image[100:-100, 100:-100, 8] = cv2.imread("data/full_images/V20150710T153226/Resized/B08A.tif",
                                                      cv2.IMREAD_UNCHANGED)
        image[100:-100, 100:-100, 9] = cv2.imread("data/full_images/V20150710T153226/Resized/B09.tif",
                                                      cv2.IMREAD_UNCHANGED)
        image[100:-100, 100:-100, 10] = cv2.imread("data/full_images/V20150710T153226/Resized/B10.tif",
                                                       cv2.IMREAD_UNCHANGED)
        image[100:-100, 100:-100, 11] = cv2.imread("data/full_images/V20150710T153226/Resized/B11.tif",
                                                       cv2.IMREAD_UNCHANGED)
        image[100:-100, 100:-100, 12] = cv2.imread("data/full_images/V20150710T153226/Resized/B12.tif",
                                                       cv2.IMREAD_UNCHANGED)

        image = (image / np.max(image, axis=(0, 1))) * 255

        util.delete_files('./data/predicted_b/iceberg')

    num_h = math.ceil(h / 10)
    num_w = math.ceil(w / 10)

    count_ib = 0
    count_o = 0
    stride = 1.

    total = math.ceil((h / stride) * (w / stride))

    blank = np.zeros((h, w, 3))

    # points = [(c, r) for c in range(0, h, 10) for r in range(0, w, 10)]
    #
    # util.print_progress_bar(0, len(points), prefix='Progress:', suffix='Complete', length=50)
    #
    # pool = Pool(os.cpu_count()-1)
    # for i, _ in enumerate(pool.imap_unordered(Predicter(radius, image, architecture), points)):
    #     (ib, o) = i
    #
    #     count_ib += ib
    #     count_o += o
    #
    #     util.print_progress_bar(count_o + count_ib, len(points), prefix='Progress:', suffix='Complete', length=50)

    confidences = np.zeros((10980, 10980))

    for c in range(0, h, int(stride)):
        for r in range(0, w, int(stride)):
            patch = None
            left = r - radius
            right = r + radius
            upper = c - radius
            lower = c + radius

            if not util.is_band_architecture(architecture):
                patch = image.crop(box=(left, upper, right, lower))
            elif util.is_band_architecture(architecture):
                patch = image[upper + 100:lower + 100, left + 100:right + 100]

            prediction = nn.predict(np.expand_dims(np.array(patch), axis=0))

            confidences[r, c] = prediction[0][0]
            best = np.argmax(prediction[0], axis=-1)

            if best == 0:
                blank[c, r, 2] = 255
                # patch = true_rgb.crop(box=(left, upper, right, lower))
                # if not util.is_band_architecture(architecture):
                #     patch.save("./data/predicted/iceberg/ib_{}_{}.png".format(r, c), "PNG")
                # elif util.is_band_architecture(architecture):
                #     patch.save("./data/predicted_b/iceberg/ib_{}_{}.png".format(r, c), "PNG")
                patch = None
                count_ib += 1
            elif best == 1:
                # patch.save("./data/predicted/other/o_{}_{}".format(c, r), "PNG")
                count_o += 1

            util.print_progress_bar(count_o + count_ib, total, prefix='Progress:', suffix='Complete', length=50)

    np.save("confidences.npy", confidences)

    print("Total icebergs: {}".format(count_ib))
    print("Total others: {}".format(count_o))
    print("Total: {}".format(total))
    cv2.imwrite('predicted_full.png', blank)


def predict_patch(config, image_path):
    print("Starting patch predictions")
    Image.MAX_IMAGE_PIXELS = None

    nn = util.load_architecture(config)

    nn.feature_extractor.compile(loss=config['train']['loss'],
                                 optimizer='adam',
                                 metrics=['accuracy'])
    nn.load_weights()

    image = Image.open(image_path)
    patch = image.crop(box=(50, 50, 150, 150))

    prediction = nn.predict(np.expand_dims(np.array(patch), axis=0))

    print(prediction)
    print(np.argmax(prediction, axis=-1))


def predict_gen(config):
    print("Starting gen predictions")
    Image.MAX_IMAGE_PIXELS = None

    nn = util.load_architecture(config)

    nn.feature_extractor.compile(loss=config['train']['loss'],
                                 optimizer='adam',
                                 metrics=['accuracy'])
    nn.load_weights()

    training_images, training_count = util.parse_images(config["train"]["images_folder"], config['model']['labels'])
    validation_images, validation_count = util.parse_images(config["validation"]["images_folder"], config['model']['labels'])
    testing_images, testing_count = util.parse_images(config["test"]["images_folder"], config['model']['labels'])

    training_gen = train.training_generator(training_images, config, nn.normalize, shuffle=False, label=False, evalu=True)
    validation_gen = train.test_generator(validation_images, config, nn.normalize, shuffle=False, label=False)
    testing_gen = train.test_generator(testing_images, config, nn.normalize, shuffle=False, label=False)

    true_training_y = create_true_labels(training_count)
    true_validation_y = create_true_labels(validation_count)
    true_testing_y = create_true_labels(testing_count)

    print("Training count: {}".format(training_count))
    predictions = nn.feature_extractor.predict_generator(generator=training_gen, steps=len(training_gen))
    save_bad(training_gen, np.argmax(predictions, axis=-1), true_training_y)
    cnf_matrix = confusion_matrix(true_training_y, np.argmax(predictions, axis=-1))
    print(cnf_matrix)
    # plot_confusion_matrix(cnf_matrix, classes=config['model']['labels'], title='Confusion matrix, without normalization')
    print(count_per_label(training_count, predictions))

    print("Validation count: {}".format(validation_count))
    predictions = nn.feature_extractor.predict_generator(generator=validation_gen, steps=len(validation_gen))
    save_bad(validation_gen, np.argmax(predictions, axis=-1), true_validation_y)
    cnf_matrix = confusion_matrix(true_validation_y, np.argmax(predictions, axis=-1))
    print(cnf_matrix)
    # plot_confusion_matrix(cnf_matrix, classes=config['model']['labels'], title='Confusion matrix, without normalization')
    print(count_per_label(validation_count, predictions))

    print("Testing count: {}".format(testing_count))
    predictions = nn.feature_extractor.predict_generator(generator=testing_gen, steps=len(testing_gen))
    save_bad(testing_gen, np.argmax(predictions, axis=-1), true_testing_y)
    cnf_matrix = confusion_matrix(true_testing_y, np.argmax(predictions, axis=-1))
    print(cnf_matrix)
    # plot_confusion_matrix(cnf_matrix, classes=config['model']['labels'], title='Confusion matrix, without normalization')
    print(count_per_label(testing_count, predictions))


def predict_gen_ob(config):
    print("Starting gen object detection predictions")
    # Load selected architecture
    nn = util.load_architecture(config)

    optimizer = Adam(lr=nn.learning_rate, beta_1=0.9, beta_2=0.999, epsilon=1e-08, decay=0.0)
    nn.feature_extractor.compile(loss=nn.loss,
                                 optimizer=optimizer)
    nn.load_weights()

    icebergs = 0

    path = config['train']['images_folder']
    for the_file in os.listdir(path):
        file_path = os.path.join(path, the_file)
        image = cv2.imread(file_path)
        if image is not None:
            boxes = nn.predict(image)
            icebergs += len(boxes)
            image = draw_boxes(image, boxes, config['model']['labels'])
            cv2.imwrite("ob_detections/{}".format(the_file), image)
            # write_annotation_to_file(path, the_file, image, boxes, 'iceberg')

    path = config['validation']['images_folder']
    for the_file in os.listdir(path):
        file_path = os.path.join(path, the_file)
        image = cv2.imread(file_path)
        if image is not None:
            boxes = nn.predict(image)
            icebergs += len(boxes)
            image = draw_boxes(image, boxes, config['model']['labels'])
            cv2.imwrite("ob_detections/{}".format(the_file), image)
            # write_annotation_to_file(path, the_file, image, boxes, 'iceberg')

    print("Icebergs detected: {}".format(icebergs))


def predict_gen_ob_folder(config, path, save_images=False):
    print("Starting gen object detection folder predictions")
    # Load selected architecture
    nn = util.load_architecture(config)

    optimizer = Adam(lr=nn.learning_rate, beta_1=0.9, beta_2=0.999, epsilon=1e-08, decay=0.0)
    nn.feature_extractor.compile(loss=nn.loss,
                                 optimizer=optimizer)
    nn.load_weights()

    icebergs = 0
    total = len(os.listdir(path))
    util.print_progress_bar(0, total, prefix='Progress:', suffix='Complete', length=50)

    for the_file, count in zip(os.listdir(path), range(total)):
        file_path = os.path.join(path, the_file)
        image = cv2.imread(file_path)
        if image is not None:
            boxes = nn.predict(image)
            icebergs += len(boxes)
            write_annotation_to_file(path, the_file, image, boxes, 'iceberg')
            if save_images:
                image = draw_boxes(image, boxes, config['model']['labels'])
                cv2.imwrite("ob_detections/{}".format(the_file), image)

        util.print_progress_bar(count, total, prefix='Progress:', suffix='Complete', length=50)
    util.print_progress_bar(total, total, prefix='Progress:', suffix='Complete', length=50)

    print("Icebergs detected: {}".format(icebergs))


def predict_gen_ob_v3_folder(config, path, save_images=False):
    print("Starting gen object detection v3 folder prediction")
    nn = loading_v3(config)

    icebergs = 0
    total = len(os.listdir(path))
    util.print_progress_bar(0, total, prefix='Progress:', suffix='Complete', length=50)

    for the_file, count in zip(os.listdir(path), range(total)):
        file_path = os.path.join(path, the_file)
        image = cv2.imread(file_path)
        if image is not None:
            boxes = nn.predict(image)
            icebergs += len(boxes)
            write_annotation_to_file_v3(path, the_file, image, boxes, 'iceberg')
            if save_images:
                image = draw_boxes(image, boxes, config['model']['labels'])
                cv2.imwrite("ob_detections/{}".format(the_file), image)

        util.print_progress_bar(count, total, prefix='Progress:', suffix='Complete', length=50)
    util.print_progress_bar(total, total, prefix='Progress:', suffix='Complete', length=50)

    print("Icebergs detected: {}".format(icebergs))


def predict_ob_v3_image(config, image_path, evaluate=False):
    print("Starting object detection v3 image predictions")
    # Load selected architecture
    nn = loading_v3(config)

    path, filename = os.path.split(image_path)


    # image = cv2.imread(image_path)
    #
    # if image is None:
    #     raise FileNotFoundError("File {} not found".format(image_path))

    image = np.zeros((10980, 10980, 3))
    image[:, :, 0] = cv2.imread(os.path.join(path, "B04.jp2"), cv2.IMREAD_UNCHANGED)
    image[:, :, 1] = cv2.imread(os.path.join(path, "B03.jp2"), cv2.IMREAD_UNCHANGED)
    image[:, :, 2] = cv2.imread(os.path.join(path, "B02.jp2"), cv2.IMREAD_UNCHANGED)

    image_width, image_height, image_channels = image.shape
    nms_thresh = config['model']["nms_thresh"]

    valid_boxes = []

    if image_width > 256:
        # Break image up
        width_sections = int(math.ceil(image_width / 128))
        height_sections = int(math.ceil(image_height / 128))

        padded_image = image

        if not (width_sections == image_width / 128 and height_sections == image_height / 128):
            # Pad image on the right and bottom
            full_padded_width = width_sections * 128
            full_padded_height = height_sections * 128

            pad_right = full_padded_width - image_width
            pad_bottom = full_padded_height - image_height

            padded_image = cv2.copyMakeBorder(image, 0, pad_bottom, 0, pad_right, cv2.BORDER_CONSTANT, value=0)

        count = 0
        total = (width_sections - 1) * (height_sections - 1)

        for width_section in range(width_sections - 1):
            lower_width_section = width_section * 128
            higher_width_section = (width_section + 2) * 128

            for height_section in range(height_sections - 1):
                lower_height_section = height_section * 128
                higher_height_section = (height_section + 2) * 128

                boxes = nn.predict(padded_image[lower_height_section:higher_height_section,
                                                lower_width_section:higher_width_section, :])

                for box in boxes:
                    box.xmin += lower_width_section
                    box.xmax += lower_width_section
                    box.ymin += lower_height_section
                    box.ymax += lower_height_section

                valid_boxes.extend(boxes)

                count += 1
                util.print_progress_bar(count, total, prefix='Progress:', suffix='Complete', length=50)

        print("Pre-suppressed: {}".format(len(valid_boxes)))

        for index_i in range(len(valid_boxes) - 1):
            if valid_boxes[index_i].classes[0] == 0:
                continue

            for index_j in range(index_i + 1, len(valid_boxes)):
                if valid_boxes[index_j].classes[0] == 0:
                    continue

                if bbox_iou(valid_boxes[index_i], valid_boxes[index_j]) >= nms_thresh:
                    if valid_boxes[index_i].c > valid_boxes[index_j].c:
                        valid_boxes[index_j].classes[0] = 0
                    else:
                        valid_boxes[index_i].classes[0] = 0
                        break

        temp = valid_boxes
        valid_boxes = []
        for box in temp:
            if box.classes[0] > 0:
                valid_boxes.append(box)
    else:
        # Just apply prediction
        valid_boxes = nn.predict(image)

    if not evaluate:
        labeled_image = draw_boxes_v3(image, valid_boxes, ['iceberg'], 0.7, plabel=False)
        # cv2.imwrite(os.path.join(path, "labelled_{}".format(filename)), labeled_image)

        write_annotation_to_file_v3(path, filename, image, valid_boxes, 'iceberg')

        print("Icebergs detected: {}".format(len(valid_boxes)))
    else:
        return valid_boxes


def loading_v3(config):
    # Load selected architecture
    config['train']['warmup_epochs'] = 0
    nn = util.load_architecture(config)

    optimizer = Adam(lr=nn.learning_rate, beta_1=0.9, beta_2=0.999, epsilon=1e-08, decay=0.0)
    nn.feature_extractor_infer.compile(loss=nn.loss,
                                       optimizer=optimizer)
    nn.load_weights_infer()

    return nn


def create_true_labels(count):
    true_y = np.zeros((int(np.sum(count))))
    count_from = 0
    count_to = 0
    for c, idx in zip(count, range(len(count))):
        count_to += int(c)
        true_y[count_from:count_to] = idx
        count_from += int(c)

    return true_y


def count_per_label(count, predictions):
    count_from = 0
    count_to = 0

    total = []

    for c in count:
        count_to += int(c)

        total.append(np.sum(predictions[count_from:count_to], axis=0))
        count_from += int(c)

    return total


def save_bad(gen, predictions, true):
    print("Saving bad images")
    positions = np.where(predictions != true)[0]

    for pos in positions:
        data = gen.get_item_data(pos)
        item = gen.get_item(pos)
        Image.fromarray(item).save('iceberg/{}'.format(data['filename']))

# def plot_confusion_matrix(cm, classes,
#                           normalize=False,
#                           title='Confusion matrix',
#                           cmap=plt.cm.Blues):
#     """
#     This function prints and plots the confusion matrix.
#     Normalization can be applied by setting `normalize=True`.
#     """
#     plt.figure()
#     if normalize:
#         cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
#         print("Normalized confusion matrix")
#     else:
#         print('Confusion matrix, without normalization')
#
#     print(cm)
#
#     plt.imshow(cm, interpolation='nearest', cmap=cmap)
#     plt.title(title)
#     plt.colorbar()
#     tick_marks = np.arange(len(classes))
#     plt.xticks(tick_marks, classes, rotation=45)
#     plt.yticks(tick_marks, classes)
#
#     fmt = '.2f' if normalize else 'd'
#     thresh = cm.max() / 2.
#     for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
#         plt.text(j, i, format(cm[i, j], fmt),
#                  horizontalalignment="center",
#                  color="white" if cm[i, j] > thresh else "black")
#
#     plt.tight_layout()
#     plt.ylabel('True label')
#     plt.xlabel('Predicted label')
#
#     # plt.show()