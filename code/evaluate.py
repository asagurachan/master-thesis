"""
University of Copenhagen
Master Thesis

Deep Learning Pipeline For Iceberg Detection

Darrick Joo Jian Wen (NGC655)


"""

import os
import numpy as np
import predict
from generators.generator_ob_v3 import BatchGenerator as BatchGenerator_v3
from extras import util
from extras.nn_util_v3 import get_yolo_boxes, compute_ap, compute_overlap


def evaluate_image(config, image_path):
    print("Starting evaluate image")

    detections = predict.predict_ob_v3_image(config, image_path=image_path, evaluate=True)
    detections = np.expand_dims(np.expand_dims(np.array([[box.xmin, box.ymin, box.xmax, box.ymax, box.get_score()] for box in detections]), axis=0), axis=0)

    path, _ = os.path.split(image_path)

    imgs, _, _ = util.parse_annotation(os.path.join(path, "annotations"), path, config["model"]["labels"])
    annotations = None
    for img in imgs:
        if img['filename'] == image_path:
            annotations = np.expand_dims(np.expand_dims([[box['xmin'], box['ymin'], box['xmax'], box['ymax'], 1] for box in img['object']], axis=0), axis=0)
            break

    print(evaluation(detections, annotations, config["model"]["labels"]))


def evaluate_generators(config):
    print("Starting evaluate generators")

    # Load selected architecture
    nn = util.load_architecture(config)
    nn.load_weights_infer()

    object_thresh = config["model"]["object_thresh"]
    nms_thresh = config["model"]["nms_thresh"]
    input_size = config["model"]["input_size"]

    train_imgs, _, _ = util.parse_annotation(config['train']['annotations_folder'],
                                             config['train']['images_folder'],
                                             config['model']['labels'])

    valid_imgs, _, _ = util.parse_annotation(config['validation']['annotations_folder'],
                                             config['validation']['images_folder'],
                                             config['model']['labels'])

    testing_images = None
    if config['test']['annotations_folder']:
        testing_images, _, _ = util.parse_annotation(config['test']['annotations_folder'],
                                                     config['test']['images_folder'],
                                                     config['model']['labels'])

    train_gen_v3 = BatchGenerator_v3(train_imgs,
                                     anchors=nn.anchors,
                                     labels=nn.classes,
                                     downsample=32,
                                     max_box_per_image=0,
                                     batch_size=nn.batch_size,
                                     min_net_size=config['model']['min_input_size'],
                                     max_net_size=config['model']['max_input_size'],
                                     shuffle=True,
                                     jitter=0.0,
                                     norm=nn.normalize)

    valid_gen_v3 = BatchGenerator_v3(valid_imgs,
                                     anchors=nn.anchors,
                                     labels=nn.classes,
                                     downsample=32,
                                     max_box_per_image=0,
                                     batch_size=nn.batch_size,
                                     min_net_size=config['model']['min_input_size'],
                                     max_net_size=config['model']['max_input_size'],
                                     shuffle=True,
                                     jitter=0.0,
                                     norm=nn.normalize)

    all_detections, all_annotations = _evaluate_gen(nn.feature_extractor_infer,
                                                    train_gen_v3,
                                                    obj_thresh=object_thresh,
                                                    nms_thresh=nms_thresh,
                                                    net_h=input_size,
                                                    net_w=input_size)
    print(evaluation(all_detections, all_annotations, config["model"]["labels"]))

    all_detections, all_annotations = _evaluate_gen(nn.feature_extractor_infer,
                                                    valid_gen_v3,
                                                    obj_thresh=object_thresh,
                                                    nms_thresh=nms_thresh,
                                                    net_h=input_size,
                                                    net_w=input_size)
    print(evaluation(all_detections, all_annotations, config["model"]["labels"]))

    if testing_images:
        test_gen_v3 = BatchGenerator_v3(testing_images,
                                        anchors=nn.anchors,
                                        labels=nn.classes,
                                        downsample=32,
                                        max_box_per_image=0,
                                        batch_size=nn.batch_size,
                                        min_net_size=config['model']['min_input_size'],
                                        max_net_size=config['model']['max_input_size'],
                                        shuffle=True,
                                        jitter=0.0,
                                        norm=nn.normalize)

        all_detections, all_annotations = _evaluate_gen(nn.feature_extractor_infer,
                                                       test_gen_v3,
                                                       obj_thresh=object_thresh,
                                                       nms_thresh=nms_thresh,
                                                       net_h=input_size,
                                                       net_w=input_size)

        print(evaluation(all_detections, all_annotations, config["model"]["labels"]))


def _evaluate_gen(model,
                  generator,
                  obj_thresh=0.5,
                  nms_thresh=0.45,
                  net_h=416,
                  net_w=416,
                  save_path=None):
    # gather all detections and annotations
    all_detections = [[None for _ in range(generator.num_classes())] for _ in range(generator.size())]
    all_annotations = [[None for _ in range(generator.num_classes())] for _ in range(generator.size())]

    for i in range(generator.size()):
        raw_image = [generator.load_image(i)]

        # make the boxes and the labels
        pred_boxes = get_yolo_boxes(model, raw_image, net_h, net_w, generator.get_anchors(), obj_thresh, nms_thresh)[0]

        score = np.array([box.get_score() for box in pred_boxes])
        # score = np.array([box.c for box in pred_boxes])
        pred_labels = np.array([box.label for box in pred_boxes])

        if len(pred_boxes) > 0:
            pred_boxes = np.array([[box.xmin, box.ymin, box.xmax, box.ymax, box.get_score()] for box in pred_boxes])
        else:
            pred_boxes = np.array([[]])

        # sort the boxes and the labels according to scores
        score_sort = np.argsort(-score)
        pred_labels = pred_labels[score_sort]
        pred_boxes = pred_boxes[score_sort]

        # copy detections to all_detections
        for label in range(generator.num_classes()):
            all_detections[i][label] = pred_boxes[pred_labels == label, :]

        annotations = generator.load_annotation(i)

        # copy detections to all_annotations
        for label in range(generator.num_classes()):
            all_annotations[i][label] = annotations[annotations[:, 4] == label, :4].copy()

    return all_detections, all_annotations


def evaluation(all_detections,
               all_annotations,
               classes,
               iou_threshold=0.5):
    """ Evaluate a given dataset using a given model.
        modified code originally from https://github.com/fizyr/keras-retinanet

        # Arguments
            model           : The model to evaluate.
            generator       : The generator that represents the dataset to evaluate.
            iou_threshold   : The threshold used to consider when a detection is positive or negative.
            save_path       : The path to save images with visualized detections to.
        # Returns
            A dict mapping class names to mAP scores.
    """

    # compute mAP by comparing all detections and all annotations
    average_precisions = {}
    true_positives_all = 0
    false_positives_all = 0

    for label in range(len(classes)):
        false_positives = np.zeros((0,))
        true_positives = np.zeros((0,))
        scores = np.zeros((0,))
        num_annotations = 0.0

        for i in range(len(all_detections)):
            detections = all_detections[i][label]
            annotations = all_annotations[i][label]
            num_annotations += annotations.shape[0]
            detected_annotations = []

            for d in detections:
                scores = np.append(scores, d[4])

                if annotations.shape[0] == 0:
                    false_positives = np.append(false_positives, 1)
                    true_positives = np.append(true_positives, 0)
                    continue

                overlaps = compute_overlap(np.expand_dims(d, axis=0), annotations)
                assigned_annotation = np.argmax(overlaps, axis=1)
                max_overlap = overlaps[0, assigned_annotation]

                if max_overlap >= iou_threshold and assigned_annotation not in detected_annotations:
                    false_positives = np.append(false_positives, 0)
                    true_positives = np.append(true_positives, 1)
                    detected_annotations.append(assigned_annotation)
                    true_positives_all += 1
                else:
                    false_positives = np.append(false_positives, 1)
                    true_positives = np.append(true_positives, 0)
                    false_positives_all += 1

        # no annotations -> AP for this class is 0 (is this correct?)
        if num_annotations == 0:
            average_precisions[label] = 0
            continue

        # sort by score
        indices = np.argsort(-scores)
        false_positives = false_positives[indices]
        true_positives = true_positives[indices]

        # compute false positives and true positives
        false_positives = np.cumsum(false_positives)
        true_positives = np.cumsum(true_positives)

        # compute recall and precision
        recall = true_positives / num_annotations
        precision = true_positives / np.maximum(true_positives + false_positives, np.finfo(np.float64).eps)

        # compute average precision
        average_precision = compute_ap(recall, precision)
        average_precisions[label] = average_precision

    return average_precisions, true_positives_all, false_positives_all
