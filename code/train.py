"""
University of Copenhagen
Master Thesis

Deep Learning Pipeline For Iceberg Detection

Darrick Joo Jian Wen (NGC655)
"""

import numpy as np
import os
from extras import util
from extras.voc import parse_voc_annotation
from generators.generator import BatchGenerator
from generators.generator_ob import BatchGenerator as BatchGenerator_ob
from generators.generator_ob_v3 import BatchGenerator as BatchGenerator_v3


def train(config):
    print("Starting training")

    # Load selected architecture
    nn = util.load_architecture(config)

    # Load images with labels of datasets
    training_images, _ = util.parse_images(config["train"]["images_folder"], config['model']['labels'])
    validation_images, _ = util.parse_images(config["validation"]["images_folder"], config['model']['labels'])
    testing_images, _ = util.parse_images(config["test"]["images_folder"], config['model']['labels'])

    # Create generators for datasets
    training_gen = training_generator(training_images, config, nn.normalize)
    validation_gen = test_generator(validation_images, config, nn.normalize)
    testing_gen = test_generator(testing_images, config, nn.normalize)

    # Train, validate and test network on datasets
    nn.train(training_gen, validation_gen, testing_gen)


def train_via_object_detection(config):
    print("Starting training via object detection")

    # Load selected architecture
    nn = util.load_architecture(config)

    # parse annotations of the training set
    train_imgs, train_labels, _ = util.parse_annotation(config['train']['annotations_folder'],
                                                     config['train']['images_folder'],
                                                     config['model']['labels'])

    # parse annotations of the validation set, if any, otherwise split the training set
    valid_imgs, valid_labels, _ = util.parse_annotation(config['validation']['annotations_folder'],
                                                     config['validation']['images_folder'],
                                                     config['model']['labels'])

    if len(config['model']['labels']) > 0:
        overlap_labels = set(config['model']['labels']).intersection(set(train_labels.keys()))

        print('Seen labels:\t', train_labels)
        print('Given labels:\t', config['model']['labels'])
        print('Overlap labels:\t', overlap_labels)

        if len(overlap_labels) < len(config['model']['labels']):
            print('Some labels have no annotations! Please revise the list of labels in the config file!')
            return
    else:
        print('No labels are provided. Train on all seen labels.')
        config['model']['labels'] = train_labels.keys()

    generator_config = {
        'IMAGE_H': nn.input_size,
        'IMAGE_W': nn.input_size,
        'IMAGE_C': nn.channel_size,
        'GRID_H': nn.grid_h,
        'GRID_W': nn.grid_w,
        'BOX': nn.num_box,
        'LABELS': nn.classes,
        'CLASS': nn.output_size,
        'ANCHORS': nn.anchors,
        'BATCH_SIZE': nn.batch_size,
        'TRUE_BOX_BUFFER': nn.max_box_per_image,
    }

    train_gen = BatchGenerator_ob(train_imgs,
                                  generator_config,
                                  norm=nn.normalize)
    valid_gen = BatchGenerator_ob(valid_imgs,
                                  generator_config,
                                  norm=nn.normalize,
                                  jitter=False)
    nn.train(train_gen, valid_gen, None)


def train_via_object_detection_v3(config):
    print("Starting training via object detection v3")

    # Load selected architecture
    nn = util.load_architecture(config)

    # parse annotations of the training set
    train_imgs, train_labels, _ = util.parse_annotation(config['train']['annotations_folder'],
                                                        config['train']['images_folder'],
                                                        config['model']['labels'])

    # parse annotations of the validation set, if any, otherwise split the training set
    valid_imgs, valid_labels, _ = util.parse_annotation(config['validation']['annotations_folder'],
                                                        config['validation']['images_folder'],
                                                        config['model']['labels'])

    if len(config['model']['labels']) > 0:
        overlap_labels = set(config['model']['labels']).intersection(set(train_labels.keys()))

        print('Seen labels:\t', train_labels)
        print('Given labels:\t', config['model']['labels'])
        print('Overlap labels:\t', overlap_labels)

        if len(overlap_labels) < len(config['model']['labels']):
            print('Some labels have no annotations! Please revise the list of labels in the config file!')
            return
    else:
        print('No labels are provided. Train on all seen labels.')
        config['model']['labels'] = train_labels.keys()

    train_gen_v3 = BatchGenerator_v3(train_imgs,
                                     anchors=config['model']['anchors'],
                                     labels=nn.classes,
                                     downsample=32,
                                     max_box_per_image=nn.max_box_per_image,
                                     batch_size=nn.batch_size,
                                     min_net_size=config['model']['min_input_size'],
                                     max_net_size=config['model']['max_input_size'],
                                     shuffle=True,
                                     jitter=0.3,
                                     norm=nn.normalize)

    valid_gen_v3 = BatchGenerator_v3(valid_imgs,
                                     anchors=config['model']['anchors'],
                                     labels=nn.classes,
                                     downsample=32,
                                     max_box_per_image=nn.max_box_per_image,
                                     batch_size=nn.batch_size,
                                     min_net_size=config['model']['min_input_size'],
                                     max_net_size=config['model']['max_input_size'],
                                     shuffle=True,
                                     jitter=0.0,
                                     norm=nn.normalize)

    nn.train(train_gen_v3, valid_gen_v3, None)


def train_via_object_detection_v3_rac(config):
    print("Starting training via object detection v3 rac")

    # Load selected architecture
    nn = util.load_architecture(config)

    train_ints, valid_ints, labels, max_box_per_image = create_training_instances(
        config['train']['annotations_folder'],
        config['train']['images_folder'],
        "kangaroo.pkl",
        config['validation']['annotations_folder'],
        config['validation']['images_folder'],
        "",
        config['model']['labels']
    )
    print(labels)

    train_gen_v3 = BatchGenerator_v3(train_ints,
                                     anchors=config['model']['anchors'],
                                     labels=nn.classes,
                                     downsample=32,
                                     max_box_per_image=nn.max_box_per_image,
                                     batch_size=nn.batch_size,
                                     min_net_size=config['model']['min_input_size'],
                                     max_net_size=config['model']['max_input_size'],
                                     shuffle=True,
                                     jitter=0.3,
                                     norm=nn.normalize)

    valid_gen_v3 = BatchGenerator_v3(valid_ints,
                                     anchors=config['model']['anchors'],
                                     labels=nn.classes,
                                     downsample=32,
                                     max_box_per_image=nn.max_box_per_image,
                                     batch_size=nn.batch_size,
                                     min_net_size=config['model']['min_input_size'],
                                     max_net_size=config['model']['max_input_size'],
                                     shuffle=True,
                                     jitter=0.0,
                                     norm=nn.normalize)

    nn.train(train_gen_v3, valid_gen_v3, None)


def training_generator(images, config, norm, label=True, shuffle=True, evalu=False):
    """
    Create training generator
    :param images: Training dataset images
    :param config: Config
    :param norm: Normalization function
    :param label: To return labels or not when retrieving items
    :param shuffle: Shuffle the data
    :param evalu: If generator is used for evaluation
    :return: Generator
    """
    print("Loading training generator")
    train_gen = BatchGenerator(images, config=config, norm=norm, labels=label, shuffle=shuffle, jitter=not evalu, evalu=evalu)

    return train_gen


def test_generator(images, config, norm, label=True, shuffle=True):
    """
    Create testing generator
    :param images: Training dataset images
    :param config: Config
    :param norm: Normalization function
    :param label: To return labels or not when retrieving items
    :param shuffle: Shuffle the data
    :return: Generator
    """
    print("Loading testing generator")
    test_gen = BatchGenerator(images, config=config, norm=norm, labels=label, shuffle=shuffle, jitter=False, evalu=True)

    return test_gen


def create_training_instances(
    train_annot_folder,
    train_image_folder,
    train_cache,
    valid_annot_folder,
    valid_image_folder,
    valid_cache,
    labels,
):
    # parse annotations of the training set
    train_ints, train_labels = parse_voc_annotation(train_annot_folder, train_image_folder, train_cache, labels)

    # parse annotations of the validation set, if any, otherwise split the training set
    if os.path.exists(valid_annot_folder):
        valid_ints, valid_labels = parse_voc_annotation(valid_annot_folder, valid_image_folder, valid_cache, labels)
    else:
        print("valid_annot_folder not exists. Spliting the trainining set.")

        train_valid_split = int(0.8*len(train_ints))
        np.random.shuffle(train_ints)

        valid_ints = train_ints[train_valid_split:]
        train_ints = train_ints[:train_valid_split]

    # compare the seen labels with the given labels in config.json
    if len(labels) > 0:
        overlap_labels = set(labels).intersection(set(train_labels.keys()))

        print('Seen labels: \t\t'  + str(train_labels))
        print('Given labels: \t\t' + str(labels))
        print('Overlap labels: \t' + str(list(overlap_labels)))

        # return None, None, None if some given label is not in the dataset
        if len(overlap_labels) < len(labels):
            print('Some labels have no annotations! Please revise the list of labels in the config.json.')
            return None, None, None
    else:
        print('No labels are provided. Train on all seen labels.')
        print(train_labels)
        labels = train_labels.keys()

    max_box_per_image = max([len(inst['object']) for inst in (train_ints + valid_ints)])

    return train_ints, valid_ints, sorted(labels), max_box_per_image