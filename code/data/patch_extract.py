from xml.dom import minidom
from PIL import Image
import numpy as np
import pathlib

xmldoc = minidom.parse('annotations/main.xml')
itemlist = xmldoc.getElementsByTagName('bndbox')
print("Points: {}".format(len(itemlist)))

img = Image.open('main.jpg')
num = 1
ib = 0
other = 0
countonly = False
distribution = 0

max_width = 0
max_height = 0

size = 68

for o in itemlist:
    xmin = int(o.getElementsByTagName('xmin')[0].firstChild.nodeValue)
    ymin = int(o.getElementsByTagName('ymin')[0].firstChild.nodeValue)
    xmax = int(o.getElementsByTagName('xmax')[0].firstChild.nodeValue)
    ymax = int(o.getElementsByTagName('ymax')[0].firstChild.nodeValue)

    width = xmax - xmin
    height = ymax - ymin
    if width > max_width:
        max_width = width
    if height > max_height:
        max_height = height

print("Width: {} | Height: {}".format(max_width, max_height))

data = np.empty((len(itemlist), size, size, 3), dtype="int32")
labels = np.zeros((len(itemlist), 1), dtype="int32")

# v_t_size = len(itemlist) / 2.0 * 0.2
v_t_size = 55
training_size = 182

for ib_d, o_d in zip(np.linspace(0.9, 0.1, endpoint=True, num=9), np.linspace(0.1, 0.9, endpoint=True, num=9)):
    distribution += 1
    path = "./dist_{}".format(distribution)

    pathlib.Path(path + "/test/iceberg/").mkdir(parents=True, exist_ok=True)
    pathlib.Path(path + "/test/other/").mkdir(parents=True, exist_ok=True)
    pathlib.Path(path + "/validation/iceberg/").mkdir(parents=True, exist_ok=True)
    pathlib.Path(path + "/validation/other/").mkdir(parents=True, exist_ok=True)
    pathlib.Path(path + "/train/iceberg/").mkdir(parents=True, exist_ok=True)
    pathlib.Path(path + "/train/other/").mkdir(parents=True, exist_ok=True)

    ib = 0
    other = 0
    count = np.zeros(6)

    ib_max = training_size * ib_d
    o_max = training_size * o_d
    for o in itemlist:
        xmin = int(o.getElementsByTagName('xmin')[0].firstChild.nodeValue)
        ymin = int(o.getElementsByTagName('ymin')[0].firstChild.nodeValue)
        xmax = int(o.getElementsByTagName('xmax')[0].firstChild.nodeValue)
        ymax = int(o.getElementsByTagName('ymax')[0].firstChild.nodeValue)

        cropped = img.crop((xmin, ymin, xmax, ymax))
        nimg = cropped

        if o.parentNode.getElementsByTagName('name')[0].firstChild.nodeValue == 'iceberg':
            ib += 1
            if not countonly:
                if ib % 3 == 0 and v_t_size > count[1]: #test
                    nimg.save(path + "/test/iceberg/ib_" + str(ib) + ".png")
                    count[1] += 1
                elif ib % 3 == 1 and v_t_size > count[0]: #vali
                    nimg.save(path + "/validation/iceberg/ib_" + str(ib) + ".png")
                    count[0] += 1
                elif ib_max > count[4]:  # train
                    nimg.save(path + "/train/iceberg/ib_" + str(ib) + ".png")
                    count[4] += 1
        else:
            other +=1
            if not countonly:
                if other % 3 == 0 and v_t_size > count[3]: #test
                    count[3] += 1
                    nimg.save(path + "/test/other/o_" + str(other) + ".png")
                elif other % 3 == 1 and v_t_size > count[2]: #vali
                    count[2] += 1
                    nimg.save(path + "/validation/other/o_" + str(other) + ".png")
                elif o_max > count[5]:  # train
                    nimg.save(path + "/train/other/o_" + str(other) + ".png")
                    count[5] += 1

        num += 1

img.close()

print("IB: {}".format(ib))
print("other: {}".format(other))

print("Total data: {}".format(num-1))