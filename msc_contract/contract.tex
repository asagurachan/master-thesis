\documentclass[a4paper,11pt,twoside]{article}

\usepackage[latin1]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{lmodern}
\renewcommand*\familydefault{\sfdefault}

\usepackage{bibentry}
\usepackage{url}
\nobibliography*
\usepackage{geometry}

\usepackage{wrapfig}
\usepackage[pdftex]{graphicx,color} 
\usepackage{tikz}
\usetikzlibrary{shapes,arrows,shadows}
\usetikzlibrary{mindmap,trees}
\definecolor{mygreen}{rgb}{0.553,0.682,0.063}
\definecolor{myblue}{rgb}{0.0,0.208,0.376}
\definecolor{mygray}{rgb}{0.906,0.906,0.906}
\definecolor{uni_oldenburg}{HTML}{0d59ab}

\definecolor{darkblue}{rgb}{0.0,0.0,0.8}
\definecolor{mydarkgreen}{rgb}{0.0,0.5,0.0}
\definecolor{tablehighlight}{rgb}{0.0,0.5,0.0}

\usepackage[USenglish]{babel}

\renewcommand{\topfraction}{0.9}
\renewcommand{\bottomfraction}{0.8}

% Disable single lines at the start of a paragraph
\clubpenalty = 10000
% Disable single lines at the end of a paragraph
\widowpenalty = 10000 \displaywidowpenalty = 10000 

\usepackage[
  pdftex=true,
  colorlinks=true,
  linkcolor=black,
  linktocpage=true,
  menucolor=mydarkgreen,
  urlcolor=mydarkgreen,
  citecolor=mydarkgreen]{hyperref}

\usepackage[
  margin=1mm,
  font={footnotesize,it},
  labelfont=bf,
  nooneline,
  labelsep=period,
  singlelinecheck=true]{caption}

\DeclareCaptionFont{black}{\color{black}}
\DeclareCaptionFont{mydarkgreen}{\color{mydarkgreen}}
\captionsetup{labelfont={mydarkgreen,sc},textfont=black}

% ------------------------------------------------ Values -----------------------------------------------%
\geometry{a4paper, top=20mm, left=25mm, right=25mm, bottom=20mm} %headsep=10mm, footskip=12mm
\newcommand{\changefont}[3]{\fontfamily{#1} \fontseries{#2} \fontshape{#3} \selectfont}
\changefont{cmr}{m}{n}

\newcommand{\etal}{\emph{et al.}}
\newcommand{\ttt}[1]{\texttt{#1}}
\newcommand{\red}[1]{\textcolor{red}{#1}}
\newcommand{\todo}{\red{[TODO]~}}
\newcommand{\todot}[1]{\red{[TODO]~#1~}}

\begin{document}
\title{\huge Deep Learning Pipeline For Iceberg Detection}
\author{\large{Automating iceberg detection via satellite images in remote regions.}}
\date{}
\maketitle

\section*{Motivation}

A common problem in remote sensing is the detection of ``changes'' occurring over time. As icebergs are monitored via aerial reconnaissance and shore-based detections, remote areas are not covered by these methods and as such the only viable option is via satellite. As seen it can be seen, with the classifications of icebergs, the observations can be combined with other observations for products related to marine services and climate monitoring. 

\begin{figure}[h]
\centering{
\includegraphics[width=\textwidth]{images/before_after}
}% 
\caption{Before (left) and after (right) photos of the Sulzberger Ice Shelf associated with the Japan earthquake and resulting tsunami that occurred on March 11, 2011. Taken from \url{https://www.sciencedaily.com/releases/2011/08/110808132542.htm}.}
\label{fig:iceberg}
\end{figure}

One of the state-of-the-art tools in the field of machine learning are \emph{Convolutional Neural Networks}~(\texttt{CNNs}). The key idea to consider is the variability of iceberg patches used to train the network. Training a deep \texttt{CNN} from scratch is difficult because it requires a large amount of labeled training data \cite{cnn1706}. However they tend to result in high accuracies as seen by the ImageNet project where \texttt{CNNs} achieved a error rate of 16\% in 2012 from 25\% the year before and eventually down to a few percent within the next following few years. With cut outs of satellite images, the problem is similar to labelling handwritten digits with the MNIST dataset, which with a relatively simple model, can achieve a accuracy of 92\% \cite{tfminst}. 

From a practical point of view, manually labelling all the icebergs can be extremely time-consuming and this thesis aims at providing a full pipeline for iceberg detections via deep learning.

\section*{Problem Statement}

We want to automate the labelling of icebergs with high classification accuracy. Today, classifications of icebergs are reported by a mix of ships, aircraft and satellite imagery. By increasing the accuracy of iceberg classification via satellite data and machine learning, we can reduce the inefficiencies of manual labelling via ships and aircrafts and improve the safety of ships. \cite{iceberg_detection}. This thesis aims at developing a full pipeline that can be used to automatically detect icebergs in freely available satellite data. We will use deep CNNs to evaluate and improve the classification accuracies. Region of interests (ROIs) detection can be automated with R-CNN and can be combined with Long Short Term Memory (LSTM) \cite{cnn_lstm, lstm_time_series} cells which allows the usage of time-series data for improving accuracies.

This master thesis is a continuation of the project, Improving Iceberg Detection via Deep Learning, done in block 2, 2017. This is in collaboration with \texttt{climatelab.dk} and we will be working with them to acquire a good set of labelled data. An initial dataset is available to train, but part of the thesis also aims at getting the data right for use with CNNs such that they are well suited for feature learning.

In the course of the thesis, the following research questions and learning objectives shall be addressed:
\begin{enumerate}
 \item Improve the CNN accuracy of the project with given cut outs and whole satellite images.
 \item Can using R-CNN \cite{r_cnn}, Fast R-CNN \cite{fast_rcnn} or Faster R-CNN \cite{faster_rcnn, cnn_segmentation} for automatically detecting regions of interest for labelling increase performance?
 \item Can combining time-series data \cite{lstm_time_series} with R-CNN \cite{r_cnn}/Fast R-CNN \cite{fast_rcnn} /Faster R-CNN \cite{faster_rcnn, cnn_segmentation} increase labelling accuracy?
 \item Can a full pipeline given only a whole satellite image accurately detect and label icebergs accurately?
\end{enumerate}

Throughout the overall thesis, the intermediate results shall be documented in writing as part of the final thesis report. For addressing the objectives outlined above, access to appropriate computational resources (GPUs) will be provided.

\section*{Tasks}
A list of the timeline for the planned tasks below is shown in Figure~\ref{fig:timeline}. A description of the individual steps is sketched below:
\begin{enumerate}
  \item Searching for related works on image detection with CNNs.
  \item Getting familiar with various R-CNN.
  \item Acquiring dataset with labels.
  \begin{enumerate}
   \item Acquiring dataset
   \item Labelling dataset
  \end{enumerate}
  \item Implementing pipeline.
  \begin{enumerate}
    \item Implementing basic structure
    \item Implementing R-CNN to acquire sub dataset for region of interest detection
    \item Implementing LSTM with R-CNN to use time-series data
  \end{enumerate}
  \item Testing and tweaking implemented algorithms
  \item Writing the thesis report.
\end{enumerate}

\begin{figure}[h]
\centering{
\includegraphics[width=\textwidth]{images/gantt}
}% 
\caption{Timeline of planned tasks}
\label{fig:timeline}
\end{figure}

\section*{Supervision}

Main supervisor: Fabian Gieseke (\url{fabian.gieseke@di.ku.dk})\\
Co-supervisor: Kim Steenstrup Pedersen (\url{kimstp@di.ku.dk})\\
Climatelab.dk Contact: J\o rgen Bendtsen (\url{jb@climatelab.dk})\\
Climatelab.dk Contact: Jian Su (\url{jian.su@climatelab.dk})

\vspace*{-0.3cm}
\section*{Bibliography}
\newcommand\oldsection{}
\let\oldsection=\section
\renewcommand{\section}[2]{}
\bibliographystyle{plain}
\begin{footnotesize}
\bibliography{biblio}
\end{footnotesize}
\let\section=\oldsection

\end{document}
